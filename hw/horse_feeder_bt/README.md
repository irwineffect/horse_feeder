# Horse Feeder BT
The current hardware is split into a mainboard and a latch controller board for expandable I/O.

# Mainboard
 ![](/.assets/renders/main_board_front.png)
 ![](/.assets/renders/main_board_back.png)

 # Latch Controller V1
 ![](/.assets/renders/latch_controller_v1_front.png)
 ![](/.assets/renders/latch_controller_v1_back.png)