EESchema Schematic File Version 4
LIBS:display_adapter-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x16 J1
U 1 1 5D995AF8
P 3800 3050
F 0 "J1" H 3718 3967 50  0000 C CNN
F 1 "Conn_01x16" H 3718 3876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 3800 3050 50  0001 C CNN
F 3 "~" H 3800 3050 50  0001 C CNN
	1    3800 3050
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Top_Bottom J2
U 1 1 5D9981FE
P 6000 2950
F 0 "J2" H 6050 3467 50  0000 C CNN
F 1 "Conn_02x08_Top_Bottom" H 6050 3376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x08_P2.54mm_Vertical" H 6000 2950 50  0001 C CNN
F 3 "~" H 6000 2950 50  0001 C CNN
	1    6000 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2350 4150 2350
Wire Wire Line
	4000 2450 4150 2450
Wire Wire Line
	4000 2550 4150 2550
Wire Wire Line
	4000 2650 4150 2650
Wire Wire Line
	4000 2750 4150 2750
Wire Wire Line
	4000 2850 4150 2850
Wire Wire Line
	4000 2950 4150 2950
Wire Wire Line
	4000 3050 4150 3050
Wire Wire Line
	4000 3150 4150 3150
Wire Wire Line
	4000 3250 4150 3250
Wire Wire Line
	4000 3350 4150 3350
Wire Wire Line
	4000 3450 4150 3450
Wire Wire Line
	4000 3550 4150 3550
Wire Wire Line
	4000 3650 4150 3650
Wire Wire Line
	4000 3750 4150 3750
Wire Wire Line
	4000 3850 4150 3850
Wire Wire Line
	5650 2650 5800 2650
Wire Wire Line
	5650 2750 5800 2750
Wire Wire Line
	5650 2850 5800 2850
Wire Wire Line
	5650 2950 5800 2950
Wire Wire Line
	5650 3050 5800 3050
Wire Wire Line
	5650 3150 5800 3150
Wire Wire Line
	5650 3250 5800 3250
Wire Wire Line
	5650 3350 5800 3350
Wire Wire Line
	6300 2650 6450 2650
Wire Wire Line
	6300 2750 6450 2750
Wire Wire Line
	6300 2850 6450 2850
Wire Wire Line
	6300 2950 6450 2950
Wire Wire Line
	6300 3050 6450 3050
Wire Wire Line
	6300 3150 6450 3150
Wire Wire Line
	6300 3250 6450 3250
Wire Wire Line
	6300 3350 6450 3350
Text Label 4150 2350 0    50   ~ 0
GND
Text Label 4150 2450 0    50   ~ 0
VDD
Text Label 4150 2550 0    50   ~ 0
VO
Text Label 4150 2650 0    50   ~ 0
RS
Text Label 4150 2750 0    50   ~ 0
RW
Text Label 4150 2850 0    50   ~ 0
E
Text Label 4150 2950 0    50   ~ 0
DB0
Text Label 4150 3050 0    50   ~ 0
DB1
Text Label 4150 3150 0    50   ~ 0
DB2
Text Label 4150 3250 0    50   ~ 0
DB3
Text Label 4150 3350 0    50   ~ 0
DB4
Text Label 4150 3450 0    50   ~ 0
DB5
Text Label 4150 3550 0    50   ~ 0
DB6
Text Label 4150 3650 0    50   ~ 0
DB7
Text Label 4150 3750 0    50   ~ 0
LED_A
Text Label 4150 3850 0    50   ~ 0
LED_K
Text Label 5650 2650 2    50   ~ 0
GND
Text Label 5650 2750 2    50   ~ 0
VDD
Text Label 5650 2850 2    50   ~ 0
VO
Text Label 5650 2950 2    50   ~ 0
RS
Text Label 5650 3050 2    50   ~ 0
RW
Text Label 5650 3150 2    50   ~ 0
E
Text Label 5650 3250 2    50   ~ 0
DB0
Text Label 5650 3350 2    50   ~ 0
DB1
Text Label 6450 2650 0    50   ~ 0
DB2
Text Label 6450 2750 0    50   ~ 0
DB3
Text Label 6450 2850 0    50   ~ 0
DB4
Text Label 6450 2950 0    50   ~ 0
DB5
Text Label 6450 3050 0    50   ~ 0
DB6
Text Label 6450 3150 0    50   ~ 0
DB7
Text Label 6450 3250 0    50   ~ 0
LED_A
Text Label 6450 3350 0    50   ~ 0
LED_K
$EndSCHEMATC
