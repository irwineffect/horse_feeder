EESchema Schematic File Version 4
LIBS:horse_feeder-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3050 1900 2850 1900
$Comp
L Connector_Generic:Conn_02x08_Top_Bottom J?
U 1 1 5DB0ECCE
P 3250 2200
AR Path="/5DB0ECCE" Ref="J?"  Part="1" 
AR Path="/5DB02959/5DB0ECCE" Ref="J6"  Part="1" 
F 0 "J6" H 3300 2717 50  0000 C CNN
F 1 "LCD Connector" H 3300 2626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x08_P2.54mm_Vertical" H 3250 2200 50  0001 C CNN
F 3 "~" H 3250 2200 50  0001 C CNN
F 4 "ED10523-ND" H 3250 2200 50  0001 C CNN "Digikey PN"
	1    3250 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2100 2950 2100
Wire Wire Line
	2950 2000 3050 2000
Wire Wire Line
	3050 2200 2950 2200
Wire Wire Line
	3050 2300 2150 2300
Wire Wire Line
	3050 2400 2950 2400
Wire Wire Line
	3050 2500 2950 2500
Wire Wire Line
	3050 2600 2950 2600
Wire Wire Line
	3550 1900 3650 1900
Wire Wire Line
	3550 2000 3650 2000
Wire Wire Line
	3550 2100 3650 2100
Wire Wire Line
	3550 2200 3650 2200
Wire Wire Line
	3550 2300 3650 2300
Wire Wire Line
	3550 2400 3650 2400
Wire Wire Line
	3550 2500 3650 2500
Wire Wire Line
	3550 2600 3650 2600
Text Label 3650 2500 0    50   ~ 0
backlight_p
Text Label 3650 2400 0    50   ~ 0
d7
Text Label 3650 2300 0    50   ~ 0
d6
Text Label 3650 2200 0    50   ~ 0
d5
Text Label 3650 2100 0    50   ~ 0
d4
Text Label 3650 2000 0    50   ~ 0
d3
Text Label 3650 1900 0    50   ~ 0
d2
Text Label 2950 2600 2    50   ~ 0
d1
Text Label 2950 2500 2    50   ~ 0
d0
Text Label 2950 2400 2    50   ~ 0
enable
Text Label 2950 2200 2    50   ~ 0
register_select
Text Label 3650 2600 0    50   ~ 0
backlight_n
Wire Wire Line
	2850 1550 2850 1900
$Comp
L power:GND #PWR?
U 1 1 5DB0ECF9
P 2450 1600
AR Path="/5DB0ECF9" Ref="#PWR?"  Part="1" 
AR Path="/5DB02959/5DB0ECF9" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 2450 1350 50  0001 C CNN
F 1 "GND" H 2455 1427 50  0000 C CNN
F 2 "" H 2450 1600 50  0001 C CNN
F 3 "" H 2450 1600 50  0001 C CNN
	1    2450 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1550 2450 1550
Wire Wire Line
	2450 1550 2450 1600
$Comp
L Device:R_POT_US RV1
U 1 1 5DB103F5
P 2150 3550
F 0 "RV1" H 2083 3596 50  0000 R CNN
F 1 "R_POT_US" H 2083 3505 50  0000 R CNN
F 2 "horse_feeder:3386F-1-103TLF" H 2150 3550 50  0001 C CNN
F 3 "~" H 2150 3550 50  0001 C CNN
F 4 "3386F-103TLF-ND" H 2150 3550 50  0001 C CNN "Digikey PN"
	1    2150 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3550 2450 3550
Wire Wire Line
	2150 3400 2150 3250
Wire Wire Line
	2150 3700 2150 3850
$Comp
L power:GND #PWR060
U 1 1 5DB11A9D
P 2150 3850
F 0 "#PWR060" H 2150 3600 50  0001 C CNN
F 1 "GND" H 2155 3677 50  0000 C CNN
F 2 "" H 2150 3850 50  0001 C CNN
F 3 "" H 2150 3850 50  0001 C CNN
	1    2150 3850
	1    0    0    -1  
$EndComp
Text Label 2450 3550 0    50   ~ 0
contrast
NoConn ~ 2950 2500
NoConn ~ 2950 2600
NoConn ~ 3650 1900
NoConn ~ 3650 2000
$Comp
L horse_feeder:74LVC4245A U5
U 1 1 5DB19DAD
P 6650 2750
F 0 "U5" H 6650 3565 50  0000 C CNN
F 1 "74LVC4245A" H 6650 3474 50  0000 C CNN
F 2 "Package_SO:TSSOP-24_4.4x7.8mm_P0.65mm" H 6650 3250 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74LVC4245A.pdf" H 6500 3300 50  0001 C CNN
F 4 "1727-2878-ND" H 6650 2750 50  0001 C CNN "Digikey PN"
	1    6650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3450 6150 3450
Wire Wire Line
	6200 3550 6150 3550
Wire Wire Line
	6150 3450 6150 3550
Connection ~ 6150 3550
Wire Wire Line
	6150 3550 6150 3700
$Comp
L power:GND #PWR067
U 1 1 5DB1AD4C
P 6150 3700
F 0 "#PWR067" H 6150 3450 50  0001 C CNN
F 1 "GND" H 6155 3527 50  0000 C CNN
F 2 "" H 6150 3700 50  0001 C CNN
F 3 "" H 6150 3700 50  0001 C CNN
	1    6150 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3450 7150 3450
Wire Wire Line
	7150 3450 7150 3500
$Comp
L power:GND #PWR070
U 1 1 5DB1B38E
P 7150 3500
F 0 "#PWR070" H 7150 3250 50  0001 C CNN
F 1 "GND" H 7155 3327 50  0000 C CNN
F 2 "" H 7150 3500 50  0001 C CNN
F 3 "" H 7150 3500 50  0001 C CNN
	1    7150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2200 6150 2200
Wire Wire Line
	6150 2200 6150 2100
$Comp
L power:+5V #PWR066
U 1 1 5DB1BA8C
P 6150 2100
F 0 "#PWR066" H 6150 1950 50  0001 C CNN
F 1 "+5V" H 6165 2273 50  0000 C CNN
F 2 "" H 6150 2100 50  0001 C CNN
F 3 "" H 6150 2100 50  0001 C CNN
	1    6150 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2300 7150 2300
Wire Wire Line
	7150 2300 7150 2200
Wire Wire Line
	7100 2200 7150 2200
Wire Wire Line
	7150 2200 7150 2100
Connection ~ 7150 2200
$Comp
L power:+3.3V #PWR069
U 1 1 5DB1CD3F
P 7150 2100
F 0 "#PWR069" H 7150 1950 50  0001 C CNN
F 1 "+3.3V" H 7165 2273 50  0000 C CNN
F 2 "" H 7150 2100 50  0001 C CNN
F 3 "" H 7150 2100 50  0001 C CNN
	1    7150 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2600 5900 2600
Wire Wire Line
	7100 2450 7200 2450
Wire Wire Line
	7200 2450 7200 2300
Wire Wire Line
	7200 2300 7300 2300
Wire Wire Line
	7300 2300 7300 2350
$Comp
L power:GND #PWR071
U 1 1 5DB24685
P 7300 2350
F 0 "#PWR071" H 7300 2100 50  0001 C CNN
F 1 "GND" H 7305 2177 50  0000 C CNN
F 2 "" H 7300 2350 50  0001 C CNN
F 3 "" H 7300 2350 50  0001 C CNN
	1    7300 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2450 6100 2450
Wire Wire Line
	6100 2450 6100 2300
Wire Wire Line
	6100 2300 6000 2300
Wire Wire Line
	6000 2300 6000 2350
$Comp
L power:GND #PWR064
U 1 1 5DB250DB
P 6000 2350
F 0 "#PWR064" H 6000 2100 50  0001 C CNN
F 1 "GND" H 6005 2177 50  0000 C CNN
F 2 "" H 6000 2350 50  0001 C CNN
F 3 "" H 6000 2350 50  0001 C CNN
	1    6000 2350
	-1   0    0    -1  
$EndComp
Text Label 2950 2100 2    50   ~ 0
contrast
Text Label 5900 2600 2    50   ~ 0
register_select
Wire Wire Line
	5900 2700 6200 2700
Wire Wire Line
	5900 2800 6200 2800
Wire Wire Line
	5900 2900 6200 2900
Wire Wire Line
	5900 3100 6200 3100
Wire Wire Line
	5900 3200 6200 3200
Wire Wire Line
	5900 3300 6200 3300
Wire Wire Line
	6200 3000 5900 3000
Wire Wire Line
	7400 2600 7100 2600
Wire Wire Line
	7100 2700 7400 2700
Wire Wire Line
	7100 2800 7400 2800
Wire Wire Line
	7100 2900 7400 2900
Wire Wire Line
	7100 3100 7400 3100
Wire Wire Line
	7100 3200 7400 3200
Wire Wire Line
	7400 3000 7100 3000
Text Label 5900 2700 2    50   ~ 0
enable
Text Label 5900 2800 2    50   ~ 0
d4
Text Label 5900 2900 2    50   ~ 0
d5
Text Label 5900 3000 2    50   ~ 0
d6
Text Label 5900 3100 2    50   ~ 0
d7
Text Label 2000 2300 2    50   ~ 0
read_nwrite
Wire Wire Line
	2150 2300 2150 2150
Connection ~ 2150 2300
Wire Wire Line
	2150 2300 2000 2300
Text HLabel 7400 2600 2    50   Input ~ 0
lcd_register_select
Text HLabel 7400 2700 2    50   Input ~ 0
lcd_enable
Text HLabel 7400 2800 2    50   Input ~ 0
lcd_d4
Text HLabel 7400 2900 2    50   Input ~ 0
lcd_d5
Text HLabel 7400 3000 2    50   Input ~ 0
lcd_d6
Text HLabel 7400 3100 2    50   Input ~ 0
lcd_d7
Wire Wire Line
	4000 3650 4000 3850
Wire Wire Line
	4000 3850 4100 3850
Text Label 4100 3850 0    50   ~ 0
backlight_p
Text Label 4100 4000 0    50   ~ 0
backlight_n
Wire Wire Line
	4100 4000 4000 4000
Wire Wire Line
	4000 4000 4000 4050
Wire Wire Line
	4000 4850 4000 5000
$Comp
L power:GND #PWR063
U 1 1 5DB3FEFB
P 4000 5000
F 0 "#PWR063" H 4000 4750 50  0001 C CNN
F 1 "GND" H 4005 4827 50  0000 C CNN
F 2 "" H 4000 5000 50  0001 C CNN
F 3 "" H 4000 5000 50  0001 C CNN
	1    4000 5000
	1    0    0    -1  
$EndComp
Text HLabel 3050 4650 0    50   Input ~ 0
lcd_backlight
NoConn ~ 5900 3300
Wire Wire Line
	7400 3300 7400 3400
$Comp
L power:GND #PWR072
U 1 1 5DD65C5B
P 7400 3400
F 0 "#PWR072" H 7400 3150 50  0001 C CNN
F 1 "GND" H 7405 3227 50  0000 C CNN
F 2 "" H 7400 3400 50  0001 C CNN
F 3 "" H 7400 3400 50  0001 C CNN
	1    7400 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R26
U 1 1 5DDB6819
P 4000 4200
F 0 "R26" H 4070 4246 50  0000 L CNN
F 1 "0" H 4070 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3930 4200 50  0001 C CNN
F 3 "~" H 4000 4200 50  0001 C CNN
F 4 "RMCF0603ZT0R00CT-ND" H 4000 4200 50  0001 C CNN "Digikey PN"
	1    4000 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4350 4000 4450
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5DDBE270
P 3900 4650
AR Path="/5DDBE270" Ref="Q?"  Part="1" 
AR Path="/5D42F764/5DDBE270" Ref="Q?"  Part="1" 
AR Path="/5DB02959/5DDBE270" Ref="Q2"  Part="1" 
F 0 "Q2" H 4106 4696 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 4106 4605 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4100 4750 50  0001 C CNN
F 3 "~" H 3900 4650 50  0001 C CNN
F 4 "2N7002-FDICT-ND" H 3900 4650 50  0001 C CNN "Digikey PN"
	1    3900 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4650 3700 4650
Text Label 2950 2000 2    50   ~ 0
lcd_power
Text HLabel 7400 3200 2    50   Input ~ 0
lcd_n_power_control
$Comp
L power:+5V #PWR065
U 1 1 5D71A9DC
P 6050 4500
F 0 "#PWR065" H 6050 4350 50  0001 C CNN
F 1 "+5V" H 6065 4673 50  0000 C CNN
F 2 "" H 6050 4500 50  0001 C CNN
F 3 "" H 6050 4500 50  0001 C CNN
	1    6050 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4500 6050 4650
$Comp
L Device:Q_PMOS_GSD Q3
U 1 1 5D71CBE1
P 5950 4850
F 0 "Q3" H 6156 4804 50  0000 L CNN
F 1 "Q_PMOS_GSD" H 6156 4895 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6150 4950 50  0001 C CNN
F 3 "" H 5950 4850 50  0001 C CNN
F 4 "DMP2004KDICT-ND" H 5950 4850 50  0001 C CNN "Digikey PN"
	1    5950 4850
	1    0    0    1   
$EndComp
Wire Wire Line
	6050 5050 6050 5150
Wire Wire Line
	6050 5150 6100 5150
Text Label 6100 5150 0    50   ~ 0
lcd_power
Text Label 5900 3200 2    50   ~ 0
n_power_control
Text Label 5650 4850 2    50   ~ 0
n_power_control
Wire Wire Line
	5750 4850 5650 4850
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 5D727B3F
P 6850 4800
F 0 "JP1" V 6804 4868 50  0000 L CNN
F 1 "SolderJumper_2_Open" V 6895 4868 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 6850 4800 50  0001 C CNN
F 3 "~" H 6850 4800 50  0001 C CNN
F 4 "~" H 6850 4800 50  0001 C CNN "Digikey PN"
	1    6850 4800
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR068
U 1 1 5D729FB2
P 6850 4500
F 0 "#PWR068" H 6850 4350 50  0001 C CNN
F 1 "+5V" H 6865 4673 50  0000 C CNN
F 2 "" H 6850 4500 50  0001 C CNN
F 3 "" H 6850 4500 50  0001 C CNN
	1    6850 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 4500 6850 4650
Wire Wire Line
	6850 4950 6850 5150
Wire Wire Line
	6850 5150 6900 5150
Text Label 6900 5150 0    50   ~ 0
lcd_power
Wire Wire Line
	7100 3300 7400 3300
Text Label 4000 3650 0    50   ~ 0
lcd_power
Text Label 2150 3250 0    50   ~ 0
lcd_power
Text Label 2150 2150 2    50   ~ 0
lcd_power
$EndSCHEMATC
