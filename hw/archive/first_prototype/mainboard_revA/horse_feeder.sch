EESchema Schematic File Version 4
LIBS:horse_feeder-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2750 1150 2950 1150
Wire Wire Line
	2750 1300 2950 1300
Wire Wire Line
	2750 1450 2950 1450
Wire Wire Line
	2750 1600 2950 1600
Wire Wire Line
	2750 1750 2950 1750
Wire Wire Line
	2750 1900 2950 1900
Wire Wire Line
	2750 2050 2950 2050
Wire Wire Line
	2750 2200 2950 2200
Wire Wire Line
	2750 2350 2950 2350
Text Label 2950 1150 0    50   ~ 0
latch_1_control
Text Label 2950 1300 0    50   ~ 0
latch_2_control
Text Label 2950 1450 0    50   ~ 0
latch_3_control
Text Label 2950 1600 0    50   ~ 0
latch_4_control
Text Label 2950 1750 0    50   ~ 0
latch_5_control
Text Label 2950 1900 0    50   ~ 0
latch_6_control
Text Label 2950 2050 0    50   ~ 0
latch_7_control
Text Label 2950 2200 0    50   ~ 0
latch_8_control
Text Label 2950 2350 0    50   ~ 0
latch_9_control
Wire Wire Line
	7950 1150 7950 1050
Wire Wire Line
	8050 1150 8050 1050
Wire Wire Line
	8050 1050 7950 1050
Connection ~ 7950 1050
Wire Wire Line
	7950 1050 7950 900 
Wire Wire Line
	8150 1150 8150 1050
Wire Wire Line
	8150 1050 8050 1050
Connection ~ 8050 1050
$Comp
L power:+3.3V #PWR012
U 1 1 5D5B6997
P 7950 900
F 0 "#PWR012" H 7950 750 50  0001 C CNN
F 1 "+3.3V" H 7965 1073 50  0000 C CNN
F 2 "" H 7950 900 50  0001 C CNN
F 3 "" H 7950 900 50  0001 C CNN
	1    7950 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 4750 7850 4850
Wire Wire Line
	7950 4750 7950 4850
Wire Wire Line
	7950 4850 7850 4850
Connection ~ 7850 4850
Wire Wire Line
	7850 4850 7850 5050
Wire Wire Line
	8050 4750 8050 4850
Wire Wire Line
	8050 4850 7950 4850
Connection ~ 7950 4850
Wire Wire Line
	8250 4750 8250 5050
$Comp
L power:GND #PWR011
U 1 1 5D5BB5CB
P 7850 5050
F 0 "#PWR011" H 7850 4800 50  0001 C CNN
F 1 "GND" H 7855 4877 50  0000 C CNN
F 2 "" H 7850 5050 50  0001 C CNN
F 3 "" H 7850 5050 50  0001 C CNN
	1    7850 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR013
U 1 1 5D5BBC2C
P 8250 5050
F 0 "#PWR013" H 8250 4800 50  0001 C CNN
F 1 "GNDA" H 8255 4877 50  0000 C CNN
F 2 "" H 8250 5050 50  0001 C CNN
F 3 "" H 8250 5050 50  0001 C CNN
	1    8250 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1550 7150 1550
Text Label 7150 1550 2    50   ~ 0
BOOT0
Wire Wire Line
	8750 3250 8900 3250
Text Label 8900 3250 0    50   ~ 0
BOOT1
Wire Wire Line
	7350 2650 7150 2650
Wire Wire Line
	7350 2750 7150 2750
Text Label 7150 2650 2    50   ~ 0
OSC_IN
Text Label 7150 2750 2    50   ~ 0
OSC_OUT
Wire Wire Line
	8750 2650 8950 2650
Wire Wire Line
	8750 2750 8950 2750
Text Label 8950 2650 0    50   ~ 0
DEBUG_SWDIO
Text Label 8950 2750 0    50   ~ 0
DEBUG_SWCLK
$Comp
L Connector:USB_B_Micro J2
U 1 1 5D5DA4D6
P 1350 3700
F 0 "J2" H 1407 4167 50  0000 C CNN
F 1 "USB_B_Micro" H 1407 4076 50  0000 C CNN
F 2 "horse_feeder:USB_Micro_B_Female_10118193-0001LF" H 1500 3650 50  0001 C CNN
F 3 "~" H 1500 3650 50  0001 C CNN
F 4 "10118193-0001LF" H 1350 3700 50  0001 C CNN "Digikey PN"
	1    1350 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3700 2500 3700
Wire Wire Line
	1650 3800 2000 3800
$Comp
L Device:R R1
U 1 1 5D5DCE93
P 2500 3550
F 0 "R1" H 2570 3596 50  0000 L CNN
F 1 "1.5k" H 2570 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2430 3550 50  0001 C CNN
F 3 "~" H 2500 3550 50  0001 C CNN
F 4 "A130092CT-ND" H 2500 3550 50  0001 C CNN "Digikey PN"
	1    2500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3500 1950 3500
Wire Wire Line
	2500 3400 2500 3300
$Comp
L power:+3.3V #PWR04
U 1 1 5D5DF9E4
P 2500 3300
F 0 "#PWR04" H 2500 3150 50  0001 C CNN
F 1 "+3.3V" H 2515 3473 50  0000 C CNN
F 2 "" H 2500 3300 50  0001 C CNN
F 3 "" H 2500 3300 50  0001 C CNN
	1    2500 3300
	1    0    0    -1  
$EndComp
Connection ~ 2500 3700
$Comp
L Device:R R2
U 1 1 5D5E0D73
P 3100 3700
F 0 "R2" V 2893 3700 50  0000 C CNN
F 1 "22" V 2984 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3030 3700 50  0001 C CNN
F 3 "~" H 3100 3700 50  0001 C CNN
F 4 "A130081CT-ND" H 3100 3700 50  0001 C CNN "Digikey PN"
	1    3100 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D5E1422
P 3100 3850
F 0 "R3" V 3300 3850 50  0000 C CNN
F 1 "22" V 3200 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3030 3850 50  0001 C CNN
F 3 "~" H 3100 3850 50  0001 C CNN
F 4 "A130081CT-ND" H 3100 3850 50  0001 C CNN "Digikey PN"
	1    3100 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 3800 2000 3850
Wire Wire Line
	2000 3850 2950 3850
Wire Wire Line
	2500 3700 2950 3700
Wire Wire Line
	3250 3700 3400 3700
Wire Wire Line
	3250 3850 3400 3850
Text Label 3400 3700 0    50   ~ 0
USBD_P
Text Label 3400 3850 0    50   ~ 0
USBD_N
Wire Wire Line
	1350 4100 1350 4200
Wire Wire Line
	1250 4100 1250 4200
Wire Wire Line
	1250 4200 1350 4200
Connection ~ 1350 4200
Wire Wire Line
	1350 4200 1350 4350
$Comp
L power:GND #PWR03
U 1 1 5D5EADEC
P 1350 4350
F 0 "#PWR03" H 1350 4100 50  0001 C CNN
F 1 "GND" H 1355 4177 50  0000 C CNN
F 2 "" H 1350 4350 50  0001 C CNN
F 3 "" H 1350 4350 50  0001 C CNN
	1    1350 4350
	1    0    0    -1  
$EndComp
Text Label 1950 3500 0    50   ~ 0
VBUS
Wire Notes Line
	2900 4150 3300 4150
Wire Notes Line
	3300 3350 2900 3350
Wire Notes Line
	3300 3350 3300 4150
Wire Notes Line
	2900 4150 2900 3350
Text Notes 2300 4400 0    50   ~ 0
AN4879 claims termination resistors aren't \nnecessary, try populating with 0 ohm resistors
Wire Wire Line
	900  7100 1100 7100
Wire Wire Line
	900  7200 1100 7200
Wire Wire Line
	900  7300 1100 7300
Wire Wire Line
	900  7400 1100 7400
Text Label 1100 7300 0    50   ~ 0
NRST
Text Label 7100 1350 2    50   ~ 0
NRST
Text Label 1100 7100 0    50   ~ 0
DEBUG_SWDIO
Text Label 1100 7200 0    50   ~ 0
DEBUG_SWCLK
Wire Wire Line
	1100 7400 1100 7500
$Comp
L power:GND #PWR01
U 1 1 5D60618A
P 1100 7500
F 0 "#PWR01" H 1100 7250 50  0001 C CNN
F 1 "GND" H 1105 7327 50  0000 C CNN
F 2 "" H 1100 7500 50  0001 C CNN
F 3 "" H 1100 7500 50  0001 C CNN
	1    1100 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6900 1300 6850
$Comp
L power:+3.3V #PWR02
U 1 1 5D607D46
P 1300 6850
F 0 "#PWR02" H 1300 6700 50  0001 C CNN
F 1 "+3.3V" H 1315 7023 50  0000 C CNN
F 2 "" H 1300 6850 50  0001 C CNN
F 3 "" H 1300 6850 50  0001 C CNN
	1    1300 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5D60C32F
P 10000 850
F 0 "Y1" H 10000 1118 50  0000 C CNN
F 1 "8 MHz" H 10000 1027 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 10000 850 50  0001 C CNN
F 3 "~" H 10000 850 50  0001 C CNN
F 4 "535-14944-1-ND" H 10000 850 50  0001 C CNN "Digikey PN"
	1    10000 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 850  9850 850 
Wire Wire Line
	10150 850  10150 1000
Wire Wire Line
	9800 850  9800 1000
$Comp
L Device:C C3
U 1 1 5D60C33C
P 9800 1150
F 0 "C3" H 9915 1196 50  0000 L CNN
F 1 "30p" H 9915 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9838 1000 50  0001 C CNN
F 3 "~" H 9800 1150 50  0001 C CNN
F 4 " 1276-1021-1-ND" H 9800 1150 50  0001 C CNN "Digikey PN"
	1    9800 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5D60C346
P 10150 1150
F 0 "C4" H 10265 1196 50  0000 L CNN
F 1 "30p" H 10265 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10188 1000 50  0001 C CNN
F 3 "~" H 10150 1150 50  0001 C CNN
F 4 " 1276-1021-1-ND" H 10150 1150 50  0001 C CNN "Digikey PN"
	1    10150 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 1300 9800 1550
Wire Wire Line
	9800 1550 10000 1550
Wire Wire Line
	10150 1550 10150 1300
Wire Wire Line
	10000 1700 10000 1550
Connection ~ 10000 1550
Wire Wire Line
	10000 1550 10150 1550
$Comp
L power:GND #PWR015
U 1 1 5D60C356
P 10000 1700
F 0 "#PWR015" H 10000 1450 50  0001 C CNN
F 1 "GND" H 10005 1527 50  0000 C CNN
F 2 "" H 10000 1700 50  0001 C CNN
F 3 "" H 10000 1700 50  0001 C CNN
	1    10000 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 850  10400 850 
Connection ~ 10150 850 
Wire Wire Line
	9800 850  9600 850 
Connection ~ 9800 850 
Text Label 9600 850  2    50   ~ 0
OSC_IN
Text Label 10800 850  0    50   ~ 0
OSC_OUT
$Comp
L Device:R R9
U 1 1 5D610FD5
P 10550 850
F 0 "R9" V 10343 850 50  0000 C CNN
F 1 "663" V 10434 850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10480 850 50  0001 C CNN
F 3 "~" H 10550 850 50  0001 C CNN
F 4 "311-665HRCT-ND" H 10550 850 50  0001 C CNN "Digikey PN"
	1    10550 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	10700 850  10800 850 
Wire Wire Line
	4650 1050 4450 1050
Wire Wire Line
	4650 1200 4450 1200
Wire Wire Line
	4650 1350 4450 1350
Wire Wire Line
	4650 1500 4450 1500
Wire Wire Line
	4650 1650 4450 1650
Wire Wire Line
	4650 1800 4450 1800
Text Label 4650 1050 0    50   ~ 0
btn_up
Text Label 4650 1200 0    50   ~ 0
btn_down
Text Label 4650 1500 0    50   ~ 0
btn_right
Text Label 4650 1350 0    50   ~ 0
btn_left
Text Label 4650 1650 0    50   ~ 0
btn_enter
Text Label 4650 1800 0    50   ~ 0
btn_back
Wire Wire Line
	10400 3850 10400 4050
Wire Wire Line
	10650 3850 10650 4050
$Comp
L Device:R R8
U 1 1 5D50E4B7
P 10400 4200
F 0 "R8" H 10600 4250 50  0000 R CNN
F 1 "10k" H 10600 4150 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10330 4200 50  0001 C CNN
F 3 "~" H 10400 4200 50  0001 C CNN
F 4 "RNCP0603FTD10K0CT-ND" H 10400 4200 50  0001 C CNN "Digikey PN"
	1    10400 4200
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5D50EE8A
P 10650 4200
F 0 "R10" H 10720 4246 50  0000 L CNN
F 1 "10k" H 10720 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10580 4200 50  0001 C CNN
F 3 "~" H 10650 4200 50  0001 C CNN
F 4 "RNCP0603FTD10K0CT-ND" H 10650 4200 50  0001 C CNN "Digikey PN"
	1    10650 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 4350 10400 4550
Wire Wire Line
	10650 4350 10650 4550
$Comp
L power:GND #PWR017
U 1 1 5D5198B1
P 10400 4550
F 0 "#PWR017" H 10400 4300 50  0001 C CNN
F 1 "GND" H 10405 4377 50  0000 C CNN
F 2 "" H 10400 4550 50  0001 C CNN
F 3 "" H 10400 4550 50  0001 C CNN
	1    10400 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5D519CBB
P 10650 4550
F 0 "#PWR018" H 10650 4300 50  0001 C CNN
F 1 "GND" H 10655 4377 50  0000 C CNN
F 2 "" H 10650 4550 50  0001 C CNN
F 3 "" H 10650 4550 50  0001 C CNN
	1    10650 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 3850 10750 3850
Wire Wire Line
	10400 3850 10300 3850
Text Label 10300 3850 2    50   ~ 0
BOOT0
Text Label 10750 3850 0    50   ~ 0
BOOT1
$Comp
L Device:LED D1
U 1 1 5D567B19
P 10400 2850
F 0 "D1" V 10439 2732 50  0000 R CNN
F 1 "Indicator LED" V 10348 2732 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 10400 2850 50  0001 C CNN
F 3 "~" H 10400 2850 50  0001 C CNN
F 4 "732-11402-ND" H 10400 2850 50  0001 C CNN "Digikey PN"
	1    10400 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 5D56AF70
P 10400 2400
F 0 "R7" H 10470 2446 50  0000 L CNN
F 1 "10" H 10470 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10330 2400 50  0001 C CNN
F 3 "~" H 10400 2400 50  0001 C CNN
F 4 " A130079CT-ND" H 10400 2400 50  0001 C CNN "Digikey PN"
	1    10400 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 2550 10400 2700
Wire Wire Line
	10400 2250 10400 2100
Wire Wire Line
	10400 2100 10500 2100
Wire Wire Line
	10400 3000 10400 3200
$Comp
L power:GND #PWR016
U 1 1 5D57B982
P 10400 3200
F 0 "#PWR016" H 10400 2950 50  0001 C CNN
F 1 "GND" H 10405 3027 50  0000 C CNN
F 2 "" H 10400 3200 50  0001 C CNN
F 3 "" H 10400 3200 50  0001 C CNN
	1    10400 3200
	1    0    0    -1  
$EndComp
Text Label 10500 2100 0    50   ~ 0
indicator_led
$Sheet
S 3750 900  700  1450
U 5D590F0D
F0 "buttons" 50
F1 "buttons.sch" 50
F2 "btn_up" O R 4450 1050 50 
F3 "btn_down" O R 4450 1200 50 
F4 "btn_left" O R 4450 1350 50 
F5 "btn_right" O R 4450 1500 50 
F6 "btn_enter" O R 4450 1650 50 
F7 "btn_back" O R 4450 1800 50 
F8 "btn_led_control" I R 4450 2100 50 
$EndSheet
$Comp
L Switch:SW_Push SW1
U 1 1 5D67FB98
P 5550 1550
F 0 "SW1" V 5596 1502 50  0000 R CNN
F 1 "Reset Button" V 5505 1502 50  0000 R CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5550 1750 50  0001 C CNN
F 3 "~" H 5550 1750 50  0001 C CNN
F 4 "450-1650-ND" H 5550 1550 50  0001 C CNN "Digikey PN"
	1    5550 1550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R6
U 1 1 5D680AC7
P 5550 2000
F 0 "R6" H 5620 2046 50  0000 L CNN
F 1 "1k" H 5620 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5480 2000 50  0001 C CNN
F 3 "~" H 5550 2000 50  0001 C CNN
F 4 "RNCP0603FTD1K00CT-ND" H 5550 2000 50  0001 C CNN "Digikey PN"
	1    5550 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1750 5550 1850
Wire Wire Line
	5550 2150 5550 2250
$Comp
L power:GND #PWR09
U 1 1 5D69F715
P 5550 2250
F 0 "#PWR09" H 5550 2000 50  0001 C CNN
F 1 "GND" H 5555 2077 50  0000 C CNN
F 2 "" H 5550 2250 50  0001 C CNN
F 3 "" H 5550 2250 50  0001 C CNN
	1    5550 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5D6AF338
P 6350 1600
F 0 "C2" H 6465 1646 50  0000 L CNN
F 1 "100n" H 6465 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6388 1450 50  0001 C CNN
F 3 "~" H 6350 1600 50  0001 C CNN
F 4 "1276-CL10B104KB8NNNLCT-ND" H 6350 1600 50  0001 C CNN "Digikey PN"
	1    6350 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1750 6350 1950
$Comp
L power:GND #PWR010
U 1 1 5D6BF40C
P 6350 1950
F 0 "#PWR010" H 6350 1700 50  0001 C CNN
F 1 "GND" H 6355 1777 50  0000 C CNN
F 2 "" H 6350 1950 50  0001 C CNN
F 3 "" H 6350 1950 50  0001 C CNN
	1    6350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1350 6350 1350
Wire Wire Line
	6350 1450 6350 1350
Connection ~ 6350 1350
Wire Wire Line
	8750 3350 8900 3350
Text Label 8900 3350 0    50   ~ 0
DEBUG_SWO
$Comp
L Connector_Generic:Conn_01x06 J1
U 1 1 5D6E39E4
P 700 7200
F 0 "J1" H 618 6675 50  0000 C CNN
F 1 "Debug" H 618 6766 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 700 7200 50  0001 C CNN
F 3 "~" H 700 7200 50  0001 C CNN
F 4 "~" H 700 7200 50  0001 C CNN "Digikey PN"
	1    700  7200
	-1   0    0    1   
$EndComp
Wire Wire Line
	900  6900 1300 6900
Wire Wire Line
	900  7000 1100 7000
Text Label 1100 7000 0    50   ~ 0
DEBUG_SWO
$Sheet
S 3800 2800 550  650 
U 5D4FC786
F0 "power" 50
F1 "power.sch" 50
$EndSheet
Wire Wire Line
	5000 2950 5000 3100
$Comp
L Device:R R4
U 1 1 5D4EDCC6
P 5000 3250
F 0 "R4" H 4850 3300 50  0000 L CNN
F 1 "402k_1%" H 4600 3200 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 3250 50  0001 C CNN
F 3 "~" H 5000 3250 50  0001 C CNN
F 4 "RMCF0603FT402KCT-ND" H 5000 3250 50  0001 C CNN "Digikey PN"
	1    5000 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5D4F38B5
P 5000 3800
F 0 "R5" H 4850 3850 50  0000 L CNN
F 1 "100k_1%" H 4600 3750 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4930 3800 50  0001 C CNN
F 3 "~" H 5000 3800 50  0001 C CNN
F 4 "RMCF0603FT100KCT-ND" H 5000 3800 50  0001 C CNN "Digikey PN"
	1    5000 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3400 5000 3500
Wire Wire Line
	5000 3950 5000 4000
$Comp
L power:GND #PWR08
U 1 1 5D4FF0FA
P 5000 4100
F 0 "#PWR08" H 5000 3850 50  0001 C CNN
F 1 "GND" H 5005 3927 50  0000 C CNN
F 2 "" H 5000 4100 50  0001 C CNN
F 3 "" H 5000 4100 50  0001 C CNN
	1    5000 4100
	1    0    0    -1  
$EndComp
Connection ~ 5000 3500
Wire Wire Line
	5000 3500 5000 3650
$Comp
L Device:C C1
U 1 1 5D504F52
P 5400 3800
F 0 "C1" H 5515 3846 50  0000 L CNN
F 1 "1u" H 5515 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5438 3650 50  0001 C CNN
F 3 "~" H 5400 3800 50  0001 C CNN
F 4 "1276-1182-1-ND" H 5400 3800 50  0001 C CNN "Digikey PN"
	1    5400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3500 5400 3650
Wire Wire Line
	5400 3950 5400 4000
Wire Wire Line
	5400 4000 5000 4000
Connection ~ 5000 4000
Wire Wire Line
	5000 4000 5000 4100
Text Label 5500 3500 0    50   ~ 0
12v_monitor
Connection ~ 5400 3500
Wire Wire Line
	5400 3500 5500 3500
Wire Wire Line
	5000 3500 5400 3500
Wire Wire Line
	8750 1350 8950 1350
$Comp
L power:+12V #PWR07
U 1 1 5D52B1FF
P 5000 2950
F 0 "#PWR07" H 5000 2800 50  0001 C CNN
F 1 "+12V" H 5015 3123 50  0000 C CNN
F 2 "" H 5000 2950 50  0001 C CNN
F 3 "" H 5000 2950 50  0001 C CNN
	1    5000 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1850 8950 1850
Wire Wire Line
	8750 1950 8950 1950
Wire Wire Line
	8750 2050 8950 2050
Text Label 8950 1850 0    50   ~ 0
SCK
Text Label 8950 1950 0    50   ~ 0
MISO
Text Label 8950 2050 0    50   ~ 0
MOSI
Wire Wire Line
	8750 1450 8950 1450
Text Label 8950 1450 0    50   ~ 0
12v_monitor
Wire Wire Line
	8750 1750 8950 1750
Text Label 8950 1750 0    50   ~ 0
RTC_CS
Text Label 8950 1350 0    50   ~ 0
RTC_INT
Wire Wire Line
	8750 3050 8950 3050
Text Label 8950 3050 0    50   ~ 0
indicator_led
Wire Wire Line
	7850 1150 7850 1050
Wire Wire Line
	7850 1050 7950 1050
$Comp
L power:+3.3VADC #PWR014
U 1 1 5D83A0F1
P 8350 900
F 0 "#PWR014" H 8500 850 50  0001 C CNN
F 1 "+3.3VADC" H 8370 1043 50  0000 C CNN
F 2 "" H 8350 900 50  0001 C CNN
F 3 "" H 8350 900 50  0001 C CNN
	1    8350 900 
	1    0    0    -1  
$EndComp
Text Label 7200 4050 2    50   ~ 0
btn_up
Text Label 7200 4150 2    50   ~ 0
btn_down
Text Label 7200 4350 2    50   ~ 0
btn_right
Text Label 7200 4250 2    50   ~ 0
btn_left
Text Label 7200 4450 2    50   ~ 0
btn_enter
Text Label 7200 4550 2    50   ~ 0
btn_back
Text Label 7200 3150 2    50   ~ 0
latch_1_control
Text Label 7200 3250 2    50   ~ 0
latch_2_control
Text Label 7200 3350 2    50   ~ 0
latch_3_control
Text Label 7200 3450 2    50   ~ 0
latch_4_control
Text Label 7200 3550 2    50   ~ 0
latch_5_control
Text Label 7200 3650 2    50   ~ 0
latch_6_control
Text Label 7200 3750 2    50   ~ 0
latch_7_control
Text Label 7200 3850 2    50   ~ 0
latch_8_control
Text Label 7200 3950 2    50   ~ 0
latch_9_control
$Sheet
S 3850 5300 600  800 
U 5DA03C15
F0 "RTC" 50
F1 "RTC.sch" 50
F2 "SCK" I R 4450 5400 50 
F3 "MISO" O R 4450 5500 50 
F4 "MOSI" I R 4450 5600 50 
F5 "RTC_INT" O R 4450 6000 50 
F6 "RTC_CS" I R 4450 5700 50 
$EndSheet
Wire Wire Line
	4450 5400 4550 5400
Wire Wire Line
	4450 5500 4550 5500
Wire Wire Line
	4450 5600 4550 5600
Wire Wire Line
	4450 5700 4550 5700
Wire Wire Line
	4450 6000 4550 6000
Text Label 4550 5400 0    50   ~ 0
SCK
Text Label 4550 5500 0    50   ~ 0
MISO
Text Label 4550 5600 0    50   ~ 0
MOSI
Text Label 4550 5700 0    50   ~ 0
RTC_CS
Text Label 4550 6000 0    50   ~ 0
RTC_INT
$Sheet
S 1300 5100 850  1000
U 5DB02959
F0 "LCD" 50
F1 "LCD.sch" 50
F2 "lcd_register_select" I R 2150 5200 50 
F3 "lcd_enable" I R 2150 5300 50 
F4 "lcd_d4" I R 2150 5400 50 
F5 "lcd_d5" I R 2150 5500 50 
F6 "lcd_d6" I R 2150 5600 50 
F7 "lcd_d7" I R 2150 5700 50 
F8 "lcd_backlight" I R 2150 6000 50 
F9 "lcd_n_power_control" I R 2150 5850 50 
$EndSheet
Wire Wire Line
	2150 5200 2350 5200
Wire Wire Line
	2150 5300 2350 5300
Wire Wire Line
	2150 5400 2350 5400
Wire Wire Line
	2150 5500 2350 5500
Wire Wire Line
	2150 5600 2350 5600
Wire Wire Line
	2150 5700 2350 5700
Wire Wire Line
	2150 6000 2350 6000
Text Label 2350 5200 0    50   ~ 0
lcd_register_select
Text Label 2350 5300 0    50   ~ 0
lcd_enable
Text Label 2350 5400 0    50   ~ 0
lcd_d4
Text Label 2350 5500 0    50   ~ 0
lcd_d5
Text Label 2350 5600 0    50   ~ 0
lcd_d6
Text Label 2350 5700 0    50   ~ 0
lcd_d7
Text Label 2350 6000 0    50   ~ 0
lcd_backlight
Wire Wire Line
	8750 3650 8950 3650
Wire Wire Line
	8750 3750 8950 3750
Wire Wire Line
	8750 3850 8950 3850
Wire Wire Line
	8750 3950 8950 3950
Text Label 8950 3650 0    50   ~ 0
lcd_d4
Text Label 8950 3750 0    50   ~ 0
lcd_d5
Text Label 8950 3850 0    50   ~ 0
lcd_d6
Text Label 8950 3950 0    50   ~ 0
lcd_d7
Wire Wire Line
	8750 3550 8950 3550
Wire Wire Line
	8750 3450 8950 3450
Text Label 8950 3550 0    50   ~ 0
lcd_register_select
Text Label 8950 3450 0    50   ~ 0
lcd_enable
Wire Wire Line
	8750 4150 8950 4150
Text Label 8950 4150 0    50   ~ 0
lcd_backlight
Wire Wire Line
	8750 3150 8900 3150
NoConn ~ 1650 3900
$Sheet
S 1350 850  1400 1600
U 5D42F764
F0 "latches" 50
F1 "latches.sch" 50
F2 "latch_5_control" I R 2750 1750 50 
F3 "latch_9_control" I R 2750 2350 50 
F4 "latch_4_control" I R 2750 1600 50 
F5 "latch_3_control" I R 2750 1450 50 
F6 "latch_8_control" I R 2750 2200 50 
F7 "latch_7_control" I R 2750 2050 50 
F8 "latch_2_control" I R 2750 1300 50 
F9 "latch_6_control" I R 2750 1900 50 
F10 "latch_1_control" I R 2750 1150 50 
F11 "latch_0_control" I R 2750 1000 50 
$EndSheet
Wire Wire Line
	2750 1000 2950 1000
Text Label 2950 1000 0    50   ~ 0
latch_0_control
Wire Wire Line
	4650 2100 4450 2100
Text Label 4650 2100 0    50   ~ 0
btn_led_control
Text Label 8900 3150 0    50   ~ 0
btn_led_control
Wire Wire Line
	2150 5850 2350 5850
Text Label 2350 5850 0    50   ~ 0
lcd_n_power_control
Text Label 8950 4050 0    50   ~ 0
lcd_n_power_control
$Comp
L MCU_ST_STM32F1:STM32F103RCTx U1
U 1 1 5D6A7FD9
P 8050 2950
F 0 "U1" H 8050 4000 50  0000 C CNN
F 1 "STM32F103RCTx" H 8050 4150 50  0000 C CNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 7450 1250 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00191185.pdf" H 8050 2950 50  0001 C CNN
F 4 "497-17384-1-ND" H 8050 2950 50  0001 C CNN "Digikey PN"
	1    8050 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 1050 8250 1050
Wire Wire Line
	8250 1050 8250 1150
Connection ~ 8150 1050
Wire Wire Line
	8050 4850 8150 4850
Wire Wire Line
	8150 4850 8150 4750
Connection ~ 8050 4850
Wire Wire Line
	6350 1350 7350 1350
Text Label 8950 2450 0    50   ~ 0
USBD_N
Text Label 8950 2550 0    50   ~ 0
USBD_P
Wire Wire Line
	8750 2550 8950 2550
Wire Wire Line
	8750 2450 8950 2450
Wire Wire Line
	7200 3050 7350 3050
Wire Wire Line
	7200 3150 7350 3150
Wire Wire Line
	7200 3250 7350 3250
Wire Wire Line
	7200 3350 7350 3350
Wire Wire Line
	7200 3450 7350 3450
Wire Wire Line
	7200 3550 7350 3550
Wire Wire Line
	7200 3750 7350 3750
Wire Wire Line
	7200 3850 7350 3850
Wire Wire Line
	7200 4050 7350 4050
Wire Wire Line
	7200 4150 7350 4150
Wire Wire Line
	7200 4250 7350 4250
Wire Wire Line
	7200 4350 7350 4350
Wire Wire Line
	7200 4450 7350 4450
Wire Wire Line
	7200 4550 7350 4550
Wire Wire Line
	8750 2250 8950 2250
Wire Wire Line
	8750 2350 8950 2350
Text Label 8950 2250 0    50   ~ 0
UART_TX
Text Label 8950 2350 0    50   ~ 0
UART_RX
Wire Wire Line
	8750 4050 8950 4050
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5D783234
P 2350 7000
F 0 "J3" H 2268 6575 50  0000 C CNN
F 1 "Conn_01x04" H 2268 6666 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2350 7000 50  0001 C CNN
F 3 "~" H 2350 7000 50  0001 C CNN
F 4 "~" H 2350 7000 50  0001 C CNN "Digikey PN"
	1    2350 7000
	-1   0    0    1   
$EndComp
Wire Wire Line
	2550 6900 2650 6900
Wire Wire Line
	2550 6800 2650 6800
Wire Wire Line
	2700 7100 2700 7150
Wire Wire Line
	2550 7100 2700 7100
Wire Wire Line
	3200 7000 3200 6950
Wire Wire Line
	2550 7000 3200 7000
Text Label 2650 6900 0    50   ~ 0
UART_TX
Text Label 2650 6800 0    50   ~ 0
UART_RX
$Comp
L power:+3.3V #PWR06
U 1 1 5D7BC376
P 3200 6950
F 0 "#PWR06" H 3200 6800 50  0001 C CNN
F 1 "+3.3V" H 3215 7123 50  0000 C CNN
F 2 "" H 3200 6950 50  0001 C CNN
F 3 "" H 3200 6950 50  0001 C CNN
	1    3200 6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5D7BC76F
P 2700 7150
F 0 "#PWR05" H 2700 6900 50  0001 C CNN
F 1 "GND" H 2705 6977 50  0000 C CNN
F 2 "" H 2700 7150 50  0001 C CNN
F 3 "" H 2700 7150 50  0001 C CNN
	1    2700 7150
	1    0    0    -1  
$EndComp
NoConn ~ 7350 2850
Wire Wire Line
	7350 3650 7200 3650
Wire Wire Line
	7350 3950 7200 3950
Text Label 7200 3050 2    50   ~ 0
latch_0_control
NoConn ~ 8750 4250
NoConn ~ 8750 4350
NoConn ~ 8750 4450
NoConn ~ 8750 4550
NoConn ~ 8750 1550
NoConn ~ 8750 1650
NoConn ~ 8750 2150
NoConn ~ 8750 2850
Wire Wire Line
	8350 900  8350 1150
Text Label 2200 3700 0    50   ~ 0
D_P
Text Label 2200 3850 0    50   ~ 0
D_N
$EndSCHEMATC
