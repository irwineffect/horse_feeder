# Horse Feeder

An open hardware/firmware project for feeding horses (or, anything). See the [wiki](https://gitlab.com/irwineffect/horse_feeder/-/wikis/) for more details.

## Firmware
Implemented in Rust using the [RTIC](https://rtic.rs/) concurrency framework. 
See [Project Status](#project-status) for a list of the currently implemented features.

## Hardware 
The hardware has a modular design: a "mainboard" with a microcontroller and user I/O, and a "latch controller" board which actuates the feeders. The circuitry is split over multiple boards to allow for easier upgrades/modifications to the latch controller board.

![](./.assets/assembled_boards_front.jpg)

### Mainboard Features
- nRF52840 Microcontroller via [Seeed Studio XIAO nRF52840](https://www.seeedstudio.com/Seeed-XIAO-BLE-nRF52840-p-5201.html) system-on-module
  - USB-C interface
  - Bluetooth BLE ready
- RGB LED for status information
- Pushbutton for manual control
- High-accuracy (2.5 ppm) RTC for timekeeping 
  - [RV-3032-C7](https://www.microcrystal.com/fileadmin/Media/Products/RTC/Datasheet/RV-3032-C7.pdf) from Micro Crystal)
  - Battery backup (supports coin cell sizes 1216 to 1225)
- I2C header for communicating with latch controllers
- Designed for 12V power input, switching regulator is rated for 32V
  - Note: currently selected capacitors are only rated for 24V and would need to be upgraded if trying to run off a 24V system

### Latch Controller V1 Features
 - Controls up to 16 actuators
 - PMOS transistors for high-side switching [DMP3085LSD](https://www.diodes.com/assets/Datasheets/DMP3085LSD.pdf) from Diodes Incorporated
   - Rated for 4.9A continuous, 20A pulsed
  - PTC fuse to protect transistors/actuators
 - [MCP23017](https://ww1.microchip.com/downloads/aemDocuments/documents/APID/ProductDocuments/DataSheets/MCP23017-Data-Sheet-DS20001952.pdf) I2C I/O expander
 - Boards are individually addressable. Up to 8 boards can be daisy-chained together, to allow control of up to 16*8=128 actuators
 - Designed for 12V power input, transistors are rated for 30V

## Project Status:
 - [x] Hardware 
   - [x] PCBs designed 
   - [x] Assembled boards
 - [ ] Firmware
   - [x] Latch controller bringup
   - [x] Button/LED bringup
   - [x] RTC bringup
   - [x] Minimum viable product
      - Push button to schedule/start feedings, push again to cancel
      - Actuate feeders morning and night until all feedings are complete
      - Return to idle mode
   - [x] Testing mode to quickly actuate all feeders
   - [x] USB interface bringup
      - Basic serial command line interface for setting the date/time
   - [ ] Settings support
      - configure frequency/times for feeding
      - latch assertion time
      - skip/disable inactive or broken actuators
      - configurable via USB interface
      - save to battery-backed RTC RAM
   - [ ] USB bootloader
   - [ ] Bluetooth bringup
      - configure settings
      - individual latch control for testing
   - [ ] Android app
   - [ ] Bluetooth bootloader

