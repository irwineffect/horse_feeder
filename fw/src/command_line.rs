use ushell::Input;
use core::{fmt::Write, ops::Index};
use heapless::{String, Vec};
use core::str::FromStr;
use rtic::Mutex;
use chrono::prelude::*;
use crate::settings::*;
use rtt_target::{rprintln};
use crate::app::{state_manager, Event, time_manager};
use crate::buttons::ButtonAction;
// rprintln!("running usb_serial_handler");
const SHELL_PROMPT: &str = "> ";
const NL: &str = "\r\n";


pub fn process_cmd(cx: &mut crate::app::usb_serial_handler::Context, cmd: &str, args: &str) {
    let shell = &mut cx.local.shell;

    match cmd {
        "cmd_rate" => {
            match u64::from_str(args) {
                Ok(rate) => {
                    *cx.local.rate = rate;
                    write!(shell, "set rate to {rate}").unwrap();
                },
                Err(_) => {shell.write_str("failed to parse rate").unwrap();},
            }
        }

        "set" => {
            let mut args: Vec<&str, 8> = args.split(" ").collect();

            if args.len() > 0 {
                match args[0] {
                    "drop_schedule" => {
                        if args[1..].len() == 0 {
                            cx.shared.settings.lock(|settings| {
                                write!(shell, "{:#?}", settings.drop_schedule).unwrap();
                            });
                        }
                        else {
                            let times: Vec<Option<NaiveTime>, 3> = args[1..].iter().map(
                                |t| { 
                                    rprintln!("{:?}", t);
                                    NaiveTime::parse_from_str(t, "%H").ok()
                                }
                            ).collect();

                            cx.shared.settings.lock(|settings| {
                                let times: &[Option<NaiveTime>] = &times;
                                settings.drop_schedule = times.try_into().unwrap();
                                write!(shell, "{:#?}", settings.drop_schedule).unwrap();
                            });
                        }
                    }

                    "min_delay" => {
                        if args[1..].len() == 0 {
                            cx.shared.settings.lock(|settings| {
                                write!(shell, "{:#?}", settings.min_delay.num_hours()).unwrap();
                            });
                        }
                        else {
                            let duration = chrono::Duration::hours(i64::from_str(args[1]).unwrap());

                            cx.shared.settings.lock(|settings| {
                                settings.min_delay = duration;
                                write!(shell, "{:#?}", settings.min_delay).unwrap();
                            });
                        }
                    }

                    unknown_setting => {
                        write!(shell, "no setting named '{}'", unknown_setting).unwrap()
                    }
                }

            }
            else {
                shell.write_str("missing parameter, need to specify a setting").unwrap();
            }
        }

        "time" => {
            if args.len() == 0 {
                let time = cx.shared.rtc_time.lock(|rtc| {
                    rtc.get_time().time()
                });

                write!(shell, "time: {time}").unwrap();
            }
            else if args.starts_with("set ") {
                let time_str = args.split(" ").nth(1).unwrap_or("");

                let time = NaiveTime::parse_from_str(time_str, "%H:%M:%S");
                
                if let Ok(time) = time {
                    let date = cx.shared.rtc_time.lock(|rtc| {
                        rtc.get_time().date()
                    });

                    let datetime = date.and_time(time);

                    time_manager::spawn(Some(datetime)).unwrap();

                    write!(shell, "set time to: {}", time);
                }
                else {

                    write!(shell, "failed to parse time: {:?}", time);
                }


            }
            else {
                shell.write_str("unexpected parameters").unwrap();
            }
        }

        "date" => {
            if args.len() == 0 {
                let time = cx.shared.rtc_time.lock(|rtc| {
                    rtc.get_time().date()
                });

                write!(shell, "time: {time}").unwrap();
            }
            else if args.starts_with("set ") {
                let time_str = args.split(" ").nth(1).unwrap_or("");

                let date = NaiveDate::parse_from_str(time_str, "%Y-%m-%d");
                
                if let Ok(date) = date {
                    let time = cx.shared.rtc_time.lock(|rtc| {
                        rtc.get_time().time()
                    });
                    
                    let datetime = date.and_time(time);

                    time_manager::spawn(Some(datetime)).unwrap();

                    write!(shell, "set date to: {}", date);
                }
                else {

                    write!(shell, "failed to parse date: {:?}", date);
                }
            }
            else {
                shell.write_str("unexpected parameters").unwrap();
            }

        }

        "datetime" => {
            if args.len() == 0 {
                let time = cx.shared.rtc_time.lock(|rtc| {
                    rtc.get_time()
                });

                write!(shell, "time: {time}").unwrap();
            }
            else {
                shell.write_str("unexpected parameters").unwrap();
            }

        }
        "press" => {
            state_manager::spawn(Some(Event::Button(ButtonAction::Pressed))).unwrap();
        },
        "longpress" => {
            state_manager::spawn(Some(Event::Button(ButtonAction::LongPress))).unwrap();
        },
        _ => {write!(shell, "unrecognized cmd: '{}'", &cmd).unwrap();}

    }


}