use chrono::NaiveDateTime;
use nrf52840_pac::acl::acl::unused0::W;
use rtt_target::debug_rprintln;
use crate::settings::DropSchedule;
use heapless::Vec;

const MCP23017_BASE_ADDR: u8 = 0x20;

pub const PIN_MAP: [mcp230xx::Mcp23017; 16] = [
            mcp230xx::Mcp23017::A0,
            mcp230xx::Mcp23017::A1,
            mcp230xx::Mcp23017::A2,
            mcp230xx::Mcp23017::A3,
            mcp230xx::Mcp23017::A4,
            mcp230xx::Mcp23017::A5,
            mcp230xx::Mcp23017::A6,
            mcp230xx::Mcp23017::A7,

            mcp230xx::Mcp23017::B0,
            mcp230xx::Mcp23017::B1,
            mcp230xx::Mcp23017::B2,
            mcp230xx::Mcp23017::B3,
            mcp230xx::Mcp23017::B4,
            mcp230xx::Mcp23017::B5,
            mcp230xx::Mcp23017::B6,
            mcp230xx::Mcp23017::B7,
    ];

#[derive(Debug, Copy, Clone)]
pub struct ScheduledDrop {
    pub latch: usize,
    pub time: NaiveDateTime,
}

pub struct DropQueue {
    queue: heapless::spsc::Queue<ScheduledDrop, {(crate::NUM_LATCHES + 1).next_power_of_two()}>,
}

impl DropQueue {
    pub fn new() -> Self {
        Self {
            queue: heapless::spsc::Queue::new(),
        }
    }

    pub fn clear(&mut self) {
        for _ in 0..self.queue.len() {
            self.queue.dequeue();
        }
    }

    pub fn peek(&self) -> Option<&ScheduledDrop> {
        self.queue.peek()
    }

    pub fn dequeue(&mut self) -> Option<ScheduledDrop> {
        self.queue.dequeue()
    }

    pub fn enqueue(&mut self) -> Option<ScheduledDrop> {
        self.queue.dequeue()
    }

    pub fn is_empty(&self) -> bool {
        self.queue.is_empty()
    }

    pub fn schedule_drops(&mut self, schedule: DropSchedule, min_delay: chrono::Duration, current_time: NaiveDateTime) {

        let day_iter = (0..crate::NUM_LATCHES*2)
            .map(|d| {
                current_time.date() + chrono::Duration::days(d.try_into().unwrap())
            });

        let mut drop_iter = day_iter.flat_map(|day| {
            schedule.iter().filter_map(|x| *x).map(move |time| {
                day.and_time(time)
            })
        });

        let drops = drop_iter
            .skip_while(|drop| drop < &(current_time + min_delay)) // Skip drop times that are less than min_delay from now
            .take(crate::NUM_LATCHES); // Only need NUM_LATCH drops from the iterator

        for (latch, time) in drops.enumerate() {
            let drop = ScheduledDrop {
                time,
                latch,
            };
            debug_rprintln!("scheduling drop: {:?}", drop);
            self.queue.enqueue(drop).unwrap();
        }
    }

    pub fn schedule_test(&mut self, current_time: NaiveDateTime) {
        let mut i = 0;
        let mut j = 0;
        while i < crate::NUM_LATCHES {
            let next_drop = (current_time) +
                chrono::Duration::seconds((5+j*2).try_into().unwrap());
            if next_drop > current_time {
                let drop = ScheduledDrop {
                    time: next_drop,
                    latch: i,
                };
                debug_rprintln!("scheduling drop: {:?}", drop);
                self.queue.enqueue(drop).unwrap();

                i += 1;
            }
            j += 1;
        }
    }
}

pub fn detect_io_boards(i2c: ()) -> Vec<u8, 8> {

    Vec::new()
}
