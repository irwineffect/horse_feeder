use chrono::NaiveTime;
use chrono::Timelike;
use modular_bitfield::prelude::*;

pub type DropSchedule = [Option<NaiveTime>; 3];

#[derive(Debug, Clone, Copy)]
pub struct Settings {
    ///
    pub drop_schedule: DropSchedule,
    /// Minimal time between when the horse feeder is activated and the first
    /// drop can occur.
    pub min_delay: chrono::Duration,

    pub assert_time: chrono::Duration,

    pub latch_enables: [bool; crate::NUM_LATCHES]

}

#[bitfield]
#[derive(BitfieldSpecifier)]
pub struct LatchEnable {
    enable: u16,
}

impl LatchEnable {
    pub fn is_latch_enabled(&self, i: u8) -> bool {
        if u32::from(i) >= u16::BITS {
            debug_assert!(false);
            false
        }
        else {
            (self.enable() & (1 << i)) != 0

        }
    }

    pub fn set_latch_enabled(&mut self, i: u8, enable: bool) {
        if u32::from(i) >= u16::BITS {
            debug_assert!(false);
        }
        else {
            let enable = if enable {
                self.enable() | (1 << i)
            }
            else {
                self.enable() & !(1 << i)
            };
            self.set_enable(enable);
        }
    }
}

impl Default for LatchEnable {
    fn default() -> Self {
        Self::new().with_enable(0xFFFF)
    }
}


#[bitfield]
pub struct NVSettings {
    /// Enable control for individual latches. A latch can be disabled if it is
    /// not working, scheduling will skip disabled latch and move to the next
    /// available latch.
    #[bits=16]
    pub latch_enable: LatchEnable,
    /// When actuating a latch, how many milliseconds the latch should be
    /// asserted
    pub assert_time: u8,
    /// Indicates droptime 1 is enabled.
    pub drop_enable_1: bool,
    /// Indicates droptime 2 is enabled.
    pub drop_enable_2: bool,
    /// Indicates droptime 3 is enabled.
    pub drop_enable_3: bool,
    #[skip]
    __ : B1,
    /// First drop time. Indicates hour of the drop. Only valid if the
    /// corresponding drop_enable_[n] is true.
    pub drop_time_1: B4,
    /// Second drop time. Indicates hour of the drop. Only valid if the
    /// corresponding drop_enable_[n] is true.
    pub drop_time_2: B4,
    /// Third drop time. Indicates hour of the drop. Only valid if the
    /// corresponding drop_enable_[n] is true.
    pub drop_time_3: B4,
    /// Minimal time (in hours) between when the horse feeder is activated and the first
    /// drop can occur.
    pub min_delay: B4,
    #[skip]
    __ : B4,
}

impl NVSettings {
    pub fn get_drop_time(&self, i: usize) -> Option<u8> {
        match i {
            0 => {
                if self.drop_enable_1() {
                    Some(self.drop_time_1())
                }
                else {
                    None
                }
            },
            1 => {
                if self.drop_enable_2() {
                    Some(self.drop_time_2())
                }
                else {
                    None
                }
            },
            2 => {
                if self.drop_enable_3() {
                    Some(self.drop_time_3())
                }
                else {
                    None
                }
            },
            _ => { unreachable!() }
        }
    }
}

impl From<Settings> for NVSettings {
    fn from(value: Settings) -> Self {
        let mut latch_enables: LatchEnable = LatchEnable::new();
        for (i,e) in value.latch_enables.iter().enumerate() {
            latch_enables.set_latch_enabled(i.try_into().unwrap(), *e);
        }
        Self::new()
            .with_assert_time(value.assert_time.num_milliseconds().try_into().unwrap())
            .with_min_delay(value.min_delay.num_hours().try_into().unwrap())
            .with_latch_enable(latch_enables)
            .with_drop_enable_1(value.drop_schedule[0].is_some())
            .with_drop_enable_2(value.drop_schedule[1].is_some())
            .with_drop_enable_3(value.drop_schedule[2].is_some())
            .with_drop_time_1(value.drop_schedule[0].unwrap_or(NaiveTime::from_hms_opt(0, 0, 0).unwrap()).hour().try_into().unwrap())
            .with_drop_time_2(value.drop_schedule[1].unwrap_or(NaiveTime::from_hms_opt(0, 0, 0).unwrap()).hour().try_into().unwrap())
            .with_drop_time_3(value.drop_schedule[2].unwrap_or(NaiveTime::from_hms_opt(0, 0, 0).unwrap()).hour().try_into().unwrap())
    }
}

impl From<NVSettings> for Settings {
    fn from(value: NVSettings) -> Self {
        let mut latch_enables = [false; crate::NUM_LATCHES];
        for i in 0..crate::NUM_LATCHES {
            latch_enables[i] = value.latch_enable().is_latch_enabled(i.try_into().unwrap());

        }

        let drop_schedule = [
            value.get_drop_time(0).map(|t| NaiveTime::from_hms_opt(t.into(),0,0).unwrap()),
            value.get_drop_time(1).map(|t| NaiveTime::from_hms_opt(t.into(),0,0).unwrap()),
            value.get_drop_time(2).map(|t| NaiveTime::from_hms_opt(t.into(),0,0).unwrap()),
        ];


        Self {
            assert_time: chrono::Duration::milliseconds(value.assert_time().into()),
            min_delay: chrono::Duration::hours(value.min_delay().into()),
            latch_enables,
            drop_schedule,
        }
    }
}




impl core::default::Default for Settings {
    fn default() -> Self {
        Self {
            drop_schedule: [
                    Some(NaiveTime::from_hms_opt(8, 0, 0).unwrap()),
                    Some(NaiveTime::from_hms_opt(20, 0, 0).unwrap()),
                    None,
            ],
            min_delay: chrono::Duration::hours(6),
            assert_time: chrono::Duration::milliseconds(100),
            latch_enables: [true; crate::NUM_LATCHES],
        }
    }
}
