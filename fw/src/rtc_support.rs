
use rv_3032_c7;
use crate::app::ExternalI2cProxy;

pub fn configure_rv_3032_c7(rtc: &mut rv_3032_c7::Rv3032C7<ExternalI2cProxy>) -> Result<(), rv_3032_c7::Error>
{
    match rtc.get_backup_mode()? {
        rv_3032_c7::registers::BackupSwitchoverMode::DirectSwitching => {
            return Ok(())
        },
        _ => {rtc.set_backup_mode(rv_3032_c7::registers::BackupSwitchoverMode::DirectSwitching)?;}

    }

    Ok(())
}