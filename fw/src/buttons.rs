use embedded_hal::digital::v2::InputPin;

pub struct DebouncedPin<T: InputPin> {
    input_pin: T,
    integrator: u8,
    integration_time: u8,
    output: bool,
    invert: bool,
}

impl<T: InputPin> DebouncedPin<T> {
    pub const fn new(input_pin: T, integration_time: u8, invert: bool) -> Self {
        Self {
            input_pin,
            integrator: 0,
            integration_time,
            output: false,
            invert,
        }
    }

    pub fn poll(&mut self) -> Result<(), T::Error> {
        let input_pin_state: bool = if self.invert { self.input_pin.is_low() } else {self.input_pin.is_high()}?;
        

        // update integrator
        match input_pin_state {
            true => {
                self.integrator = self.integrator.saturating_add(1);
            },

            false => {
                self.integrator = self.integrator.saturating_sub(1);
            }
        }

        // set output state based on the integrator's value
        if self.integrator >= self.integration_time {
            self.output = true;
            self.integrator = self.integration_time;
        }
        else if self.integrator == 0 {
            self.output = false;
        }
        else {
            // integrator is between the limits, maintain output at the previous
            // value
        }

        Ok(())
    }
}

impl<T: InputPin>  InputPin for DebouncedPin<T> {
    type Error = T::Error;

    fn is_high(&self) -> Result<bool, Self::Error> {
        Ok(self.output)
    }

    fn is_low(&self) -> Result<bool, Self::Error> {
        Ok(!self.output)
    }
}


#[derive(Debug, Copy, Clone)]
pub enum ButtonAction {
    Pressed,
    LongPress,
}

pub struct Button {
    press_timer: u8,
    longpress_time: u8,

}

impl Button {
    pub const fn new(longpress_time: u8) -> Self {
        Self {
            press_timer: 0,
            longpress_time,
        }
    }

    pub fn update(&mut self, pin_state: bool) -> Option<ButtonAction> {
        let action;

        if pin_state { // button is pressed
            // Keep track of how long it has been pressed
            self.press_timer = self.press_timer.saturating_add(1);

            // If button has been pressed long enough, then we consider it a longpress
            action = if self.press_timer == self.longpress_time {
                Some(ButtonAction::LongPress)
            }
            else { 
                None
            };
        }
        else { // button is released

            // If press_timer is 0, the button was not just recently pressed
            action = if self.press_timer == 0 {
                //nothing to do
                None
            }
            else if self.press_timer < self.longpress_time {
                // Button was pressed for a while, but not longer than longpress
                // time, so this was a normal button push
                Some(ButtonAction::Pressed)
            }
            else {
                // A longpress was just released, no action to report
                None
            };
            self.press_timer = 0;
        }
        action
    }
}

