#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub struct RGB {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub enum LedPattern {
    Static(RGB),
    Breath(RGB, u8), // color, rate
    Blink(RGB, u8),  // color, rate
}

pub struct LedState {
    counter: u8,
    pattern: LedPattern,
}

impl LedState {
    pub const fn new() -> Self {
        Self {
            counter: 0,
            pattern: LedPattern::Static(RGB { red: 0, green: 0, blue: 0 }),
        }
    }

    pub fn configure(&mut self, new_pattern: &LedPattern) {

    }

    pub fn execute(&mut self) -> (bool, bool, bool) {

        let new_led_states = match &self.pattern  {
            LedPattern::Static(rgb) => {
                let r = rgb.red > self.counter;
                let g = rgb.green > self.counter;
                let b = rgb.blue > self.counter;
                (r, g, b)
            }
            _ => todo!()
        };

        self.counter += 1;
        if self.counter > 100 {
            self.counter = 0;
        }

        new_led_states
    }
}

pub fn pattern_from_state(state: crate::app::State) -> LedPattern {
    match state {
        crate::app::State::Init => {
            LedPattern::Static(RGB {
                red: 25,
                green: 25,
                blue: 25,
            })
        },
        crate::app::State::Idle => {
            LedPattern::Static(RGB {
                red: 50,
                green: 0,
                blue: 0,
            })
        },
        crate::app::State::Running => {
            LedPattern::Static(RGB {
                red: 0,
                green: 50,
                blue: 0,
            })
        },
        crate::app::State::Testing => {
            LedPattern::Static(RGB {
                red: 0,
                green: 0,
                blue: 50,
            })
        },
    }
}