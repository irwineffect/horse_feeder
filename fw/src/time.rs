use chrono::{Datelike, naive::NaiveDateTime, naive::NaiveDate, naive::NaiveTime};
use hal::Rtc;
use nrf52840_hal as hal;
use systick_monotonic::fugit::ExtU32;

pub struct RtcTime {
    pub rtc:  hal::rtc::Rtc<hal::pac::RTC1>,
    pub rtc_overflow_counter: hal::pac::TIMER1,
    pub epoch: NaiveDateTime,
}

impl RtcTime {
    // Attempts to perform a coherent read of the RTC time and RTC overflow
    // count registers. Will panic if it can't get a coherent read after several
    // attempts.
    pub fn get_raw(&self) -> (u32, u32) {
        for _ in 0..2 {
            // First read the overflow counter
            self.rtc_overflow_counter.tasks_capture[0].write(|w| unsafe { w.bits(1) });
            let rtc_overflow_count = self.rtc_overflow_counter.cc[0].read().bits();

            // Then read the rtc time
            let rtc_time = self.rtc.get_counter();

            // Then, read the overflow counter again to see if we got a coherent read.
            self.rtc_overflow_counter.tasks_capture[0].write(|w| unsafe { w.bits(1) });
            let rtc_overflow_count_2 = self.rtc_overflow_counter.cc[0].read().bits();

            if rtc_overflow_count_2 == rtc_overflow_count {
                return (rtc_time, rtc_overflow_count)
            }
        }

        panic!("Failed to get coherent read of RTC time");
    }

    pub fn get_time(&self) -> NaiveDateTime {
        const RTC_TICK_RATE: u32 = 8; // Hz
        const RTC_COUNTER_BITS: u32 = 24;
        const MILLISECONDS_PER_RTC_TICK: u32 = 1000/RTC_TICK_RATE;
        const SECONDS_PER_OVERFLOW: u32 = 2u32.pow(RTC_COUNTER_BITS) / RTC_TICK_RATE;

        let (rtc_time, rtc_overflow_count) = self.get_raw();

        let time_since_epoch = chrono::Duration::milliseconds((rtc_time * MILLISECONDS_PER_RTC_TICK).into()) 
            + chrono::Duration::seconds((rtc_overflow_count * SECONDS_PER_OVERFLOW).into());

        return self.epoch + time_since_epoch;
    }

    pub fn set_datetime(&mut self, new_time: &NaiveDateTime) {
        // Halt/clear the RTC, clear the overflow counter
        self.rtc.disable_counter();
        self.rtc.clear_counter();
        self.rtc_overflow_counter.tasks_clear.write(|w| w.tasks_clear().set_bit());

        // Set the new time
        self.epoch = *new_time;

        // Start the RTC
        self.rtc.enable_counter();
    }

    pub fn set_time(&mut self, new_time: &NaiveTime) {
        self.set_datetime(&self.epoch.date().and_time(*new_time));
    }

    pub fn set_date(&mut self, new_time: &NaiveDate) {
        self.set_datetime(&new_time.and_time(self.epoch.time()));
    }
}