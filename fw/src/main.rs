#![no_main]
#![no_std]

#![allow(dead_code)]
#![allow(unused_imports)]

use nrf52840_hal as _; // memory layout
use panic_rtt_target as _;
use rtic::app;

mod buttons;
mod indication;
mod time;
mod latches;
mod command_line;
mod settings;
mod rtc_support;

const CORE_FREQ: u32 = 64_000_000;
const NUM_LATCHES: usize = 16;
// Hardcoded latch assert time (in milliseconds)
// const LATCH_ASSERT_TIME: u64 = 100; 



#[app(device = nrf52840_pac, dispatchers=[RTC1, RTC2, PWM1, PWM2])]
mod app {

    use embedded_hal::blocking::serial;
    use hal::clocks::{ExternalOscillator, LfOscStarted};
    use mcp230xx::Mcp23017;
    use rtt_target::{rprint, rprintln, rdbg};
    use embedded_hal::digital::v2::{InputPin, OutputPin};
    use systick_monotonic::*;
    use systick_monotonic::fugit::RateExtU64;
    use nrf52840_hal as hal;
    use nrf52840_hal::prelude::*;
    use crate::buttons::Button;
    use crate::buttons::ButtonAction;
    use crate::buttons::DebouncedPin;
    use crate::indication::LedState;
    use crate::indication::LedPattern;
    use crate::indication::RGB;
    use crate::indication;
    use crate::time::RtcTime;
    use crate::latches::*;
    use crate::settings::*;
    use crate::rtc_support::*;
    use heapless::Vec;
    use chrono::{Datelike, naive::NaiveDateTime};
    use mcp230xx;
    use usb_device;
    use usbd_serial;
    use ushell;
    use nb;
    use shared_bus;
    use rv_3032_c7::{Rv3032C7, DateTimeAccess};

    pub type ExternalI2c = hal::twim::Twim::<hal::pac::TWIM0>;
    pub type ExternalI2cProxy = shared_bus::I2cProxy<'static, shared_bus::AtomicCheckMutex<ExternalI2c>>;

    pub struct ExternalI2CDevices {
        io: mcp230xx::Mcp230xx::<ExternalI2cProxy, mcp230xx::Mcp23017>,
        rtc: Option<rv_3032_c7::Rv3032C7<ExternalI2cProxy>>,
    }

    #[derive(Debug, Copy, Clone)]
    pub enum Event {
        Button(ButtonAction)
    }

    #[derive(Debug, Eq, PartialEq, Copy, Clone)]
    pub enum State {
        Init,
        Idle,
        Running,
        Testing,
    }

    pub struct PseudoSerial
    {
        tx: heapless::spsc::Producer<'static, u8, 128>,
        rx: heapless::spsc::Consumer<'static, u8, 128>,
    }

    impl embedded_hal::serial::Read<u8> for PseudoSerial {
        type Error = ();
        fn read(&mut self) -> Result<u8, nb::Error<Self::Error>> {
            self.rx.dequeue().ok_or(nb::Error::WouldBlock)
        }
    }

    impl embedded_hal::serial::Write<u8> for PseudoSerial {
        type Error = ();
        fn write(&mut self, b: u8) -> Result<(), nb::Error<Self::Error>> {
            self.tx.enqueue(b).or(Err(nb::Error::WouldBlock))
        }

        fn flush(&mut self) -> Result<(), nb::Error<Self::Error>> {
            while self.tx.len() > 0 {
                rprintln!("flushing!");
            }

            Ok(())
        }
    }

    #[monotonic(binds = SysTick, default = true)]
    type Mono = Systick<1000>;

    #[shared]
    struct Shared {
        rtc_time: RtcTime,
        scheduled_drops: DropQueue,
        latch_pins: [usize; crate::NUM_LATCHES],
        settings: Settings,
        external_i2c_devices: ExternalI2CDevices,
    }

    #[local]
    struct Local {
        button_pin: DebouncedPin<hal::gpio::p0::P0_28<hal::gpio::Input<hal::gpio::PullUp>>>,
        led_pwm: hal::pwm::Pwm<hal::pac::PWM0>,
        console_down: rtt_target::DownChannel,
        usb_dev: usb_device::device::UsbDevice<'static, hal::usbd::Usbd<hal::usbd::UsbPeripheral<'static>>>,
        usb_serial: usbd_serial::SerialPort<'static, hal::usbd::Usbd<hal::usbd::UsbPeripheral<'static>>>,

        serial_tx_consumer: heapless::spsc::Consumer<'static, u8, 128>,
        // serial_tx_producer: heapless::spsc::Producer<'static, u8, 128>,
        // serial_rx_consumer: heapless::spsc::Consumer<'static, u8, 128>,
        serial_rx_producer: heapless::spsc::Producer<'static, u8, 128>,
        shell: ushell::UShell<PseudoSerial, ushell::autocomplete::NoAutocomplete, ushell::history::NoHistory, 128>,
    }

    #[init(local = [
        clocks: Option<hal::clocks::Clocks<ExternalOscillator, ExternalOscillator, LfOscStarted>> = None,
        usb_bus: Option<usb_device::bus::UsbBusAllocator<hal::usbd::Usbd<hal::usbd::UsbPeripheral<'static>>>> = None,
        serial_tx_queue: Option<heapless::spsc::Queue<u8, 128>> = None,
        serial_rx_queue: Option<heapless::spsc::Queue<u8, 128>> = None,
        ])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let channels = rtt_target::rtt_init!{
            up: {
                0: {
                    size: 2048
                    name: "Terminal"
                }
            }
            down: {
                0: {
                    size: 128
                    mode: BlockIfFull
                    name: "Terminal"
                }
            }
        };
        rtt_target::set_print_channel(channels.up.0);

        rprintln!("Starting init");

        let systick = cx.core.SYST;

        let mut mono = systick_monotonic::Systick::new(systick, crate::CORE_FREQ);

        // Configure clocks
        let clocks: &'static _ = cx.local.clocks.insert(hal::Clocks::new(cx.device.CLOCK)
            .set_lfclk_src_external(hal::clocks::LfOscConfiguration::NoExternalNoBypass)
            // .set_lfclk_src_rc() // TODO: test with external oscillator, might have higher accuracy
            .start_lfclk()
            .enable_ext_hfosc()); // external high frequency oscillator required for USB.

        let p0 = hal::gpio::p0::Parts::new(cx.device.P0);
        let p1 = hal::gpio::p1::Parts::new(cx.device.P1);

        // Pull down the "read bat" GPIO pin, save the pin from long-term overvoltage
        let read_bat_pin = p0.p0_14.into_open_drain_output(hal::gpio::OpenDrainConfig::Standard0Disconnect1, hal::gpio::Level::Low);
        core::mem::drop(read_bat_pin); // Drop the pin to lock configuration

        let button_pin  = p0.p0_28.into_pullup_input();
        let button_pin = crate::buttons::DebouncedPin::new(button_pin, 5, true);

        // let led_red_pin = p1.p1_13.into_open_drain_output(hal::gpio::OpenDrainConfig::Standard0Disconnect1, hal::gpio::Level::High);
        // let led_green_pin = p1.p1_14.into_open_drain_output(hal::gpio::OpenDrainConfig::Standard0Disconnect1, hal::gpio::Level::High);
        // let led_blue_pin = p1.p1_15.into_open_drain_output(hal::gpio::OpenDrainConfig::Standard0Disconnect1, hal::gpio::Level::High);
        let led_red_pin = p1.p1_13.into_push_pull_output(hal::gpio::Level::High).degrade();
        let led_green_pin = p1.p1_14.into_push_pull_output(hal::gpio::Level::High).degrade();
        let led_blue_pin = p1.p1_15.into_push_pull_output(hal::gpio::Level::High).degrade();

        // Configure LED PWM controller
        let led_pwm = hal::pwm::Pwm::new(cx.device.PWM0);
        led_pwm
            .set_counter_mode(hal::pwm::CounterMode::Up)
            .set_period(5.khz().into())
            .set_output_pin(hal::pwm::Channel::C0, led_red_pin)
            .set_output_pin(hal::pwm::Channel::C1, led_green_pin)
            .set_output_pin(hal::pwm::Channel::C2, led_blue_pin);
        led_pwm.set_duty_on(hal::pwm::Channel::C0, 0);
        led_pwm.set_duty_on(hal::pwm::Channel::C1, 0);
        led_pwm.set_duty_on(hal::pwm::Channel::C2, 10);
        led_pwm.enable();

        let mut rtc1 = hal::rtc::Rtc::new(cx.device.RTC1, 4095).unwrap();
        rtc1.enable_event(hal::rtc::RtcInterrupt::Overflow);
        let timer1 = cx.device.TIMER1;

        // Configure timer as counter
        timer1.bitmode.write(|w| w.bitmode()._32bit());
        timer1.mode.write(|w| w.mode().low_power_counter());

        // Hook up rtc overflow event to timer's count task
        let ppi = hal::ppi::Parts::new(cx.device.PPI);
        let mut ppi0 = ppi.ppi0;
        ppi0.set_event_endpoint(rtc1.event_overflow());
        ppi0.set_task_endpoint(&timer1.tasks_count);
        ppi0.enable();

        timer1.tasks_start.write(|w| w.tasks_start().set_bit());
        rtc1.enable_counter();

        // Connect to I/O extender
        let io_i2c_reset_pin = p0.p0_03.into_push_pull_output(hal::gpio::Level::High).degrade();
        let io_i2c_sda_pin = p0.p0_04.into_floating_input().degrade();
        let io_i2c_scl_pin = p0.p0_05.into_floating_input().degrade();

        let mut i2c = hal::twim::Twim::new(
                cx.device.TWIM0,
                hal::twim::Pins {
                    scl: io_i2c_scl_pin,
                    sda: io_i2c_sda_pin,
                },
                hal::pac::twim0::frequency::FREQUENCY_A::K100
        );

        rprintln!("probing bus");
        let detected_rtc = i2c.probe(rv_3032_c7::I2C_ADDRESS).is_ok();
        rdbg!(detected_rtc);
        // rdbg!(i2c.probe(0x20));
        // rdbg!(i2c.probe(0x51));

        let external_i2c_bus: &'static _ = {
            shared_bus::new_atomic_check!(ExternalI2c = i2c).unwrap()
        };

        let io_i2c = external_i2c_bus.acquire_i2c();
        let mut io = mcp230xx::Mcp230xx::<_, mcp230xx::Mcp23017>::new(io_i2c, 0x20).unwrap();



        for pin in PIN_MAP {
            io.set_direction(pin, mcp230xx::Direction::Output).unwrap();
            io.set_output_latch(pin, mcp230xx::Level::Low).unwrap();
        }

        let current_time;
        let external_rtc = if detected_rtc {
            let mut external_rtc = rv_3032_c7::Rv3032C7::new(external_i2c_bus.acquire_i2c());
            configure_rv_3032_c7(&mut external_rtc).unwrap();

            current_time = external_rtc.datetime().unwrap();
            rprintln!("loaded time from external RTC: {:?}", current_time);
            Some(external_rtc)
        }
        else {

            current_time = chrono::NaiveDateTime::MIN;
            rprintln!("external RTC not detected, setting time to: {:?}", current_time);

            None
        };

        let external_i2c_devices = ExternalI2CDevices {
            io,
            rtc: external_rtc,
        };

        // Configure USB
        let usb_bus: &'static _ = cx.local.usb_bus.insert(usb_device::class_prelude::UsbBusAllocator::new(hal::usbd::Usbd::new(hal::usbd::UsbPeripheral::new(cx.device.USBD, clocks))));
        let mut usb_serial = usbd_serial::SerialPort::new(&usb_bus);

        let mut usb_dev = usb_device::device::UsbDeviceBuilder::new(&usb_bus, usb_device::device::UsbVidPid(0x16C0, 0x27dd))
            .manufacturer("Irwin Horse Feeders")
            .product("Horse Feeder BT")
            .serial_number("TEST_SERIAL_NUMBER")
            .device_class(usbd_serial::USB_CLASS_CDC)
            .max_packet_size_0(64)
            .build();

        let serial_tx_queue: &'static mut _ = cx.local.serial_tx_queue.insert(heapless::spsc::Queue::new());
        let serial_rx_queue: &'static mut _ = cx.local.serial_rx_queue.insert(heapless::spsc::Queue::new());
        // let mut serial_rx_queue: heapless::spsc::Queue<u8, 256> = heapless::spsc::Queue::new();

        let (serial_tx_producer, serial_tx_consumer) =  serial_tx_queue.split();
        let (serial_rx_producer, serial_rx_consumer) =  serial_rx_queue.split();


        let shell = ushell::UShell::new(
            PseudoSerial {
                tx: serial_tx_producer,
                rx: serial_rx_consumer,
            },
            ushell::autocomplete::NoAutocomplete,
            ushell::history::NoHistory);

        // Spawn all periodic tasks
        let task_spawn_delay = 100.millis();
        button_handler::spawn_after(task_spawn_delay + 1.millis(), mono.now()).unwrap();
        debug_console::spawn_after(task_spawn_delay + 3.millis()).unwrap();
        state_manager::spawn_after(task_spawn_delay + 4.millis(), None).unwrap();
        drop_checker::spawn_after(task_spawn_delay + 5.millis()).unwrap();
        usb_handler::spawn_after(task_spawn_delay + 5.millis()).unwrap();
        usb_serial_handler::spawn_after(task_spawn_delay + 6.millis()).unwrap();
        time_manager::spawn_after(task_spawn_delay + 7.millis(), None).unwrap();

        rprintln!("Init complete");

        // Return
        (
            Shared {
                rtc_time: RtcTime {
                    rtc: rtc1,
                    rtc_overflow_counter: timer1,
                    epoch: current_time,
                },
                scheduled_drops: DropQueue::new(),
                latch_pins: [0; crate::NUM_LATCHES],
                settings: Settings::default(),
                external_i2c_devices,
            },
            Local {
                button_pin,
                led_pwm,
                console_down: channels.down.0,
                usb_dev,
                usb_serial,
                // serial_tx_producer,
                serial_tx_consumer,
                serial_rx_producer,
                // serial_rx_consumer,
                shell,
            },
            init::Monotonics(mono)
        )
    }

    #[task(priority = 7, local = [usb_dev, usb_serial, serial_rx_producer, serial_tx_consumer])]
    fn usb_handler(cx: usb_handler::Context) {
        let usb_dev = cx.local.usb_dev;
        let mut usb_serial = cx.local.usb_serial;
        let mut buf = [0u8; 64];
        let mut data_received = false;
        if usb_dev.poll(&mut [usb_serial]) {
            match usb_serial.read(&mut buf) {
                Ok(count) => {
                    data_received = true;
                    // rprintln!("read {} bytes", count);
                    for b in &buf[0..count] {
                        cx.local.serial_rx_producer.enqueue(*b).unwrap();
                    }
                }

                _ => {},
            }


        }

        let mut count = 0;
        while cx.local.serial_tx_consumer.ready() {
            buf[count] = cx.local.serial_tx_consumer.dequeue().unwrap();
            count += 1;

            if count >= buf.len() {
                break;
            }
        }

        let mut write_offset = 0;
        while write_offset < count {
            match usb_serial.write(&buf[write_offset..count]) {
                Ok(len) => {
                    // rprintln!("usb wrote {} bytes", len);
                    write_offset += len;
                },
                // Ok(len) => {
                //     rprintln!("usb wrote {} bytes", len);
                // },
                Err(e) => {rprintln!("{:?}", e);},
                _ => {},
            }
        }

        usb_handler::spawn_after(1.millis()).unwrap();
    }

    #[task(capacity=3, shared=[external_i2c_devices, rtc_time])]
    fn time_manager(mut cx: time_manager::Context, new_time: Option<NaiveDateTime>) {
        // If a new time was given externally
        if let Some(new_time) = new_time {
            cx.shared.external_i2c_devices.lock(|i2c_devices|{
                if let Some(rtc) = i2c_devices.rtc.as_mut() {
                    rtc.set_datetime(&new_time).unwrap();
                }
            });
            cx.shared.rtc_time.lock(|internal_rtc| {
                internal_rtc.set_datetime(&new_time);
            });
        }
        else {
            // We triggered ourselves, just update the internal RTC time
            let accurate_time: Option<NaiveDateTime> = cx.shared.external_i2c_devices.lock(|i2c_devices|{
                i2c_devices.rtc.as_mut().and_then(|rtc|
                    Some(rtc.datetime().unwrap())
                )
            });

            if let Some(accurate_time) = accurate_time {
                let previous_time = cx.shared.rtc_time.lock(|internal_rtc| {
                    let previous_time = internal_rtc.get_time();
                    internal_rtc.set_datetime(&accurate_time);
                    previous_time
                });

                rprintln!("RTC update, previous time: {}, new_time: {}",
                    previous_time, accurate_time);
            }


            time_manager::spawn_after(1.hours(), None).unwrap();
        }
    }

    #[task(shared= [rtc_time, settings], local = [shell, rate: u64 = 50])]
    fn usb_serial_handler(mut cx: usb_serial_handler::Context) {
        use ushell::Input;
        use core::fmt::Write;
        use heapless::String;
        use core::str::FromStr;
        // rprintln!("running usb_serial_handler");
        // let shell = cx.local.shell;
        const SHELL_PROMPT: &str = "> ";
        const NL: &str = "\r\n";

        let mut cmd_buf: String<128> = String::new();
        let mut arg_buf: String<128> = String::new();

        'shell_loop: loop {
            let r = cx.local.shell.poll();
            match r {
                Ok(Some(Input::Command((cmd, args)))) => {
                    cmd_buf = cmd.into();
                    arg_buf = args.into();
                    let cmd = cmd_buf.as_str();
                    let args = arg_buf.as_str();
                    cx.local.shell.write_str(NL).unwrap();

                    crate::command_line::process_cmd(&mut cx, cmd, args);


                    cx.local.shell.write_str(NL).unwrap();
                    cx.local.shell.write_str(SHELL_PROMPT).unwrap();
                }
                Ok(Some(Input::Control(c))) => {
                    rprintln!("{:#?}", c);
                    cx.local.shell.write_str(NL).unwrap();
                    cx.local.shell.write_str(SHELL_PROMPT).unwrap();
                }
                Err(ushell::ShellError::WouldBlock) => {break 'shell_loop}
                // Err(e) => {rprintln!("{:?}", e)}
                _ => {}
            }
        }


        // Assume the USB task probably has some work to do
        // if usb_handler::spawn().is_err() {
        //     rprintln!("failed to spawn usb_handler");
        // }

        usb_serial_handler::spawn_after(cx.local.rate.millis()).ok();
    }

    // #[idle(local = [usb_dev, usb_serial])]
    // fn idle(cx: idle::Context) -> ! {

    //     rprintln!("Entered idle");
    //     let usb_dev = cx.local.usb_dev;
    //     let mut usb_serial = cx.local.usb_serial;

    //     loop {
    //         if !usb_dev.poll(&mut [usb_serial]) {
    //             continue;
    //         }

    //         let mut buf = [0u8; 64];

    //         match usb_serial.read(&mut buf) {
    //             Ok(count) if count > 0 => {
    //                 rprintln!("read {} bytes", count);
    //                 // Echo back in upper case
    //                 for c in buf[0..count].iter_mut() {
    //                     if 0x61 <= *c && *c <= 0x7a {
    //                         *c &= !0x20;
    //                     }
    //                 }

    //                 let mut write_offset = 0;
    //                 while write_offset < count {
    //                     match usb_serial.write(&buf[write_offset..count]) {
    //                         Ok(len) if len > 0 => {
    //                             rprintln!("usb wrote {} bytes", len);
    //                             write_offset += len;
    //                         },
    //                         // Ok(len) => {
    //                         //     rprintln!("usb wrote {} bytes", len);
    //                         // },
    //                         // Err(e) => {rprintln!("{:?}", e);},
    //                         _ => {},
    //                     }
    //                 }
    //             }
    //             // Ok(count) => {rprintln!("received {} bytes", count);},
    //             // Err(e) => {rprintln!("{:?}", e);},
    //             _ => {},
    //         }
    //     }
    // }

    #[task(local = [button_pin, button: Button = Button::new(200)])]
    fn button_handler(cx: button_handler::Context, instant: <Mono as rtic::Monotonic>::Instant) {
        // rprintln!("button handler running!");
        cx.local.button_pin.poll().unwrap();

        let button_action = cx.local.button.update(cx.local.button_pin.is_high().unwrap());
        match button_action {
            Some(action) => {
                rprintln!("{:?}", action);
                state_manager::spawn(Some(Event::Button(action))).unwrap();
            },
            None => {}
        }

        let next_instant = instant + 10.millis();
        button_handler::spawn_at(next_instant, next_instant).unwrap();
    }


    #[task(capacity=5, local=[led_state: LedState = LedState::new(), led_pwm])]
    fn led_manager(mut cx: led_manager::Context, new_pattern: LedPattern) {
        use hal::pwm::Channel;

        let led_pwm = cx.local.led_pwm;
        let max_duty = led_pwm.max_duty();

        let constrain = |val: u8| {
            let val = u32::from(val.clamp(0,100)) * u32::from(max_duty) / 100u32;
            let val: u16 = val.try_into().unwrap();
            val
        };

        match new_pattern {
            LedPattern::Static(rgb) => {
                led_pwm.set_duty(Channel::C0, constrain(rgb.red));
                led_pwm.set_duty(Channel::C1, constrain(rgb.green));
                led_pwm.set_duty(Channel::C2, constrain(rgb.blue));

            },
            _ => { todo!() },
        }
    }

    #[task(shared=[rtc_time, scheduled_drops])]
    fn drop_checker(mut cx: drop_checker::Context) {
        let current_time = cx.shared.rtc_time.lock(|rtc_time| rtc_time.get_time());

        cx.shared.scheduled_drops.lock(|scheduled_drops| {
            if let Some(next_drop) = scheduled_drops.peek() {
                if current_time >= next_drop.time {
                    // spawn task to drop this latch
                    assert_latch::spawn(next_drop.latch).unwrap();
                    scheduled_drops.dequeue();
                }
            }
        });

        // Schedule next check
        drop_checker::spawn_after(500.millis()).unwrap();
    }

    #[task(capacity=16, shared=[external_i2c_devices])]
    fn assert_latch(mut cx: assert_latch::Context, pin: usize) {
        rprintln!("assert latch {}", pin);

        let pin_e = &PIN_MAP[pin];

        cx.shared.external_i2c_devices.lock(|i2c| {
            i2c.io.set_output_latch(*pin_e, mcp230xx::Level::High).unwrap();
        });

        deassert_latch::spawn_after(200.millis(), pin).unwrap();
    }

    #[task(capacity=16, shared=[external_i2c_devices])]
    fn deassert_latch(mut cx: deassert_latch::Context, pin: usize) {
        rprintln!("desassert latch {}", pin);

        let pin_e = &PIN_MAP[pin];

        cx.shared.external_i2c_devices.lock(|i2c| {
            i2c.io.set_output_latch(*pin_e, mcp230xx::Level::Low).unwrap();
        });
    }

    #[task(capacity = 5, local=[state: State = State::Init], shared=[scheduled_drops, rtc_time, settings])]
    fn state_manager(mut cx: state_manager::Context, event: Option<Event>) {
        let current_state = *cx.local.state;

        let next_state: State = match current_state {
            State::Init => {
                State::Idle
            },
            State::Idle => {
                match event {
                    Some(Event::Button(ButtonAction::Pressed)) => {
                        rprintln!("scheduling drops");
                        let current_time = cx.shared.rtc_time.lock(|rtc_time| rtc_time.get_time());
                        let settings = cx.shared.settings.lock(|settings| *settings);
                        cx.shared.scheduled_drops.lock(|drops| drops.schedule_drops(settings.drop_schedule, settings.min_delay, current_time));
                        State::Running
                    }
                    Some(Event::Button(ButtonAction::LongPress)) => {
                        rprintln!("scheduling test drops");
                        let current_time = cx.shared.rtc_time.lock(|rtc_time| rtc_time.get_time());
                        cx.shared.scheduled_drops.lock(|drops| drops.schedule_test(current_time));
                        State::Testing
                    }
                    None => {
                        // Nothing to do, stay in state
                        current_state
                    }
                }
            },
            State::Running | State::Testing => {
                match event {
                    Some(Event::Button(ButtonAction::Pressed)) |
                    Some(Event::Button(ButtonAction::LongPress)) => {
                        rprintln!("canceling drops");
                        cx.shared.scheduled_drops.lock(|drops| drops.clear());
                        State::Idle
                    },
                    None => {
                        // Check if we have any more scheduled drops
                        let done = cx.shared.scheduled_drops.lock(|drops| drops.is_empty());

                        if done {
                            rprintln!("All drops complete, returning to idle");
                            State::Idle
                        }
                        else {
                            current_state
                        }
                    }
                }
            },
        };

        // If the state changed, then update the LED controls
        if next_state != *cx.local.state {
            let new_led_pattern = indication::pattern_from_state(next_state);
            led_manager::spawn(new_led_pattern).unwrap();
        }

        *cx.local.state = next_state;

        // If this run of the state manager wasn't triggered by some external
        // input event, then this was a periodic state update check. In this
        // case, schedule the next check.
        if event.is_none() {
            // trigger next periodic check
            state_manager::spawn_after(250.millis(), None).unwrap();
        }
    }

    #[task(shared=[rtc_time], local=[console_down, linebuff: [u8; 128] = [0u8; 128], buff_index: usize = 0])]
    fn debug_console(mut cx: debug_console::Context) {
        let buf = cx.local.linebuff;
        let buff_index = cx.local.buff_index;

        let num_read = cx.local.console_down.read(&mut buf[*buff_index..]);

        if num_read > 0 {
            // rprintln!("read {} bytes", num_read);
            *buff_index += num_read;

            if *buff_index > 0 && buf[*buff_index -1] == b'\n' {
                let buf: &mut [u8] = &mut buf[0..*buff_index-1];
                *buff_index = 0;

                let line: &mut str = core::str::from_utf8_mut(buf).unwrap();
                line.make_ascii_lowercase();

                // rprintln!("got line: '{}'", line);

                let line: &str = line;

                let (command, parameters) = {
                    if let Some((command, parameters)) = line.split_once(" ") {
                        (command, parameters)
                    }
                    else {
                        (line, "")
                    }
                };

                match command {
                    "led" => {
                        // let (r, g, b): (u8, u8, u8) =
                        let color_vals: Vec<Option<u8>, 3> = parameters.split(",").map(|s| s.parse().ok()).collect();

                        if color_vals.len() == 3  {
                            if color_vals.iter().all(|x| x.is_some()) {
                                let color = RGB {
                                    red: color_vals[0].unwrap(),
                                    green: color_vals[1].unwrap(),
                                    blue: color_vals[2].unwrap(),
                                };

                                led_manager::spawn(LedPattern::Static(color)).unwrap();
                            }
                            else {
                                rprintln!("params could not all be parsed into u8 values: {}", parameters);
                            }
                        }
                        else {
                            rprintln!("invalid number of parameters");
                        }

                        // let colors = parameters.map(|f| f.parse());
                        // if let /*  */Some(colors) = colors {
                        //     let r: u8 = colors[0];
                        // }
                        // let r: u8 = parameters.next()?.parse()?;

                        // rprintln!("processing led command: {}", parameters);
                    },
                    "time" => {

                        if parameters.len() == 0 {
                            let time = cx.shared.rtc_time.lock(|rtc| {
                                rtc.get_time()
                            });

                            rprintln!("time: {}", time);
                        }
                        else if parameters == "raw" {
                            let (rtc_time, rtc_overflow_count) = cx.shared.rtc_time.lock(|rtc| {
                                rtc.get_raw()
                            });

                            rprintln!("rtc time: {}", rtc_time);
                            rprintln!("overflow count: {}", rtc_overflow_count );
                        }
                        else if parameters == "overflow" {
                            cx.shared.rtc_time.lock(|rtc| {
                                rtc.rtc.trigger_overflow();

                            });
                        }
                        else if parameters == "count" {
                            cx.shared.rtc_time.lock(|rtc| {
                                rtc.rtc_overflow_counter.tasks_count.write(|w| w.tasks_count().set_bit());
                            });
                        }
                        else if parameters.starts_with("set ") {
                            let time_str = parameters.split(" ").nth(1).unwrap_or("");

                            let time = NaiveDateTime::parse_from_str(time_str, "%Y-%m-%dt%H:%M:%S");

                            if let Ok(time) = time {
                                cx.shared.rtc_time.lock(|rtc| {
                                    time_manager::spawn(Some(time)).unwrap();
                                });

                                rprintln!("set time to: {}", time);
                            }
                            else {
                                rprintln!("failed to parse time: {:?}", time);
                            }

                        }
                        else {
                            rprintln!("unexpected parameters");
                        }
                    },
                    "press" => {
                        state_manager::spawn(Some(Event::Button(ButtonAction::Pressed))).unwrap();
                    },
                    "longpress" => {
                        state_manager::spawn(Some(Event::Button(ButtonAction::LongPress))).unwrap();
                    },
                    _ => {
                        rprintln!("unexpected command: {}", command);
                    }
                }

            }

        }
        debug_console::spawn_after(100.millis()).unwrap();

    }
}
