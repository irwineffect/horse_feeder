Hardware Bringup testing:
    - button
    - tricolor LED
    - 12v monitor
    - I2C IO extender

Firmware Planning:
    - build serial configuration interface
        - parameters:
            - latch assertion time
            - latch schedule
    - get BT interface running

Physical interface
    - LED pattern when idle
    - start running when short button press
        - change to a "running" pattern
        - short button press again to cancel and go back to idle
    - hold button for 5 seconds to enter testing mode
        - change to a testing pattern
        - short button press to go back to idle
    

Create rtic_scope_rtt library
    - there exists an rtic-scope library but it requires functionality I don't think I have on my xiao board (nrf52 has no EBT, and my board doesn't break out SWO debug pin). I think I'm ok with the overhead of RTT, and I think it would be convenient. I like the idea of making an attribute macro that automatically determines an index value for each task, but how to communicate this information to the host side? Maybe just start with manual method and then figure out macro later.
    THis would involve creating a GUI tool for proper visualization, should be interesting!
