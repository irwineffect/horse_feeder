# Horse Feeder BT Firmware

## Debugging
The debug header is designed to be used with a [TC2030-NL](https://www.tag-connect.com/product-category/products/cables/6-pin-target) debug connector which exposes ARM SWD pins SWCLK and SWDIO (SWO is not exposed by the XIAO SOM).

Compile and flash with [cargo-embed](https://probe.rs/docs/tools/cargo-embed/):
```bash
cargo embed
```

Firmware is using [rtt-target](https://crates.io/crates/rtt-target) for debug logging so cargo-embed can also be used to view debug logs after flashing. 